const express = require("express");
const multiparty = require("multiparty");
const fs = require("fs");
const router = express.Router();
const util = require("util");
const db = require("../src/utils/mysql");

router.post("/upload", function (req, res) {
	var stuId = req.user.id;
	var username = req.user.username;
	var name = req.user.name;
	var classa = req.user.class;
	console.log(`[${username}]上传作业!`);

	//生成multiparty对象，并配置上传目标路径
	var form = new multiparty.Form();
	//设置编辑
	form.encoding = "utf-8";
	//设置文件存储路径
	form.uploadDir = "./stuWork/";
	//上传完成后处理
	form.parse(req, function (err, fields, files) {
		console.log("#", fields);
		if (err) {
			console.log("parse error: " + err);
		} else {
			// 首先根据作业id 查询出课程名称和作业名称
			try {
				var work_id = fields.work_id[0];
			} catch {
				res.sd("后端接收[work_id]错误!");
			}
			var workName = "";
			var courseName = "";
			const sql1 =
				"select courseName,work_name from course,work,work_course where course.id=work_course.course_id and work_course.work_id=work.id and work_id=?";
			db.query(sql1, [work_id], function (err, results) {
				// 执行 SQL 语句失败
				if (err) {
					return res.sd("参数[work_id]错误!");
				}
				// 查到作业名和课程名
				if (results.length > 0) {
					workName = results[0].work_name;
					courseName = results[0].courseName;
				}

				try {
					var inputFile = files["files"][0];
				} catch {
					res.sd("后端接收文件为空!");
				}
				var sdir = "./stuWork/" + courseName + "/" + workName + "/";
				console.log("#", sdir + inputFile.originalFilename);
				// 重命名上传的文件
				fs.rename(
					inputFile.path,
					sdir +
						username +
						"-" +
						name +
						"-" +
						classa +
						"-" +
						inputFile.originalFilename,
					function (err) {
						if (err) {
							console.log("rename error: " + err);
						} else {
							console.log("rename ok");
							// 在数据库中记录学生提交过作业了
							console.log(`学生提交作业${work_id}`);
							const sql = `select * from work_stu where work_id=? and stu_id=?`;

							db.query(
								sql,
								[work_id, stuId],
								function (err, results) {
									// 执行 SQL 语句失败
									if (err) {
										return res.sd(err.message);
									}

									if (results.length > 0) {
										return res.sd(
											{ message: "提交成功" },
											0
										);
									}
									const sql = `insert into work_stu (work_id, stu_id) value(?,?);`;

									db.query(
										sql,
										[work_id, req.user.id],
										function (err, results) {
											// 执行 SQL 语句失败
											if (err) {
												return res.sd(err.message);
											}
											// 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
											if (results.affectedRows !== 1) {
												return res.sd(
													"提交失败, 联系后端, 错误日志:\n",
													results
												);
											}
											res.sd({ message: "提交成功" }, 0);
										}
									);
								}
							);
						}
					}
				);
			});
		}
	});
});
module.exports = router;
