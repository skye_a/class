const express = require("express");
const fs = require("fs");
const db = require("../src/utils/mysql");
const router = express.Router();
const v = require("../src/utils/verify");

// 下载学生上传的文件
router.get("/download", function (req, res) {
	// 验证参数
	if (v(req, ["work_id", "docName"], res) != 0) {
		return;
	}
	// 作业的id
	var work_id = req.query.work_id;
	// 下载的文件名
	var docName = req.query.docName;
	console.log("#", work_id);
	var workName = "1.txt";
	const sql = `select courseName,work_name from course,work,work_course where course.id=work_course.course_id and work_course.work_id=work.id and work_id=?`;
	db.query(sql, [work_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		// 名被占用
		if (results.length > 0) {
			workName = results[0].work_name;
			courseName = results[0].courseName;
		}

		var path = "./stuWork/" + courseName + "/" + workName + "/" + docName; // 待下载文件的路径
		console.log("#", path);
		if (fs.existsSync(path)) {
			console.log("The path exists.");
		} else {
			console.log("#nonono");
		}
        if (fs.existsSync(path)) {
			console.log("The path exists.");
		} else {
			res.status(404);
			res.sd("文件找不到! 检查[docName]是否正确!");
			return;
		}
		var f = fs.createReadStream(path);
		res.writeHead(200, {
			"Content-Type": "application/force-download",
			"Content-Disposition": "attachment; filename=",
		});

		f.pipe(res);
	});
});

// 下载老师上传的文件
router.get("/ldownload", function (req, res) {
	// 验证参数
	if (v(req, ["work_id", "docName"], res) != 0) {
		return;
	}
	// 作业的id
	var work_id = req.query.work_id;
	// 下载的文件名
	var docName = req.query.docName;
	console.log("#", work_id);
	var workName = "1.txt";
	var courseName = "1";
	const sql = `select courseName,work_name from course,work,work_course where course.id=work_course.course_id and work_course.work_id=work.id and work_id=?`;

	db.query(sql, [work_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		// 名被占用
		if (results.length > 0) {
			workName = results[0].work_name;
			courseName = results[0].courseName;
		}

		var path =
			"./stuWork/" + courseName + "/" + workName + "/target/" + docName; // 待下载文件的路径
		console.log("####", path);
		if (fs.existsSync(path)) {
			console.log("The path exists.");
		} else {
			res.status(404);
			res.sd("文件找不到! 检查[docName]是否正确!");
			return;
		}
		var f = fs.createReadStream(path);
		res.writeHead(200, {
			"Content-Type": "application/force-download",
			"Content-Disposition": "attachment; filename=12",
		});

		f.pipe(res);
	});
});

module.exports = router;
