const express = require('express')
const router = express.Router()

// 导入用户路由处理函数模块
const userHandler = require('../src/controller/users.js')

// 学生注册
router.post('/reguser', userHandler.regUser)
// 学生登录
router.post('/login', userHandler.login)
// 老师注册
router.post('/lreguser', userHandler.lregUser)
// 老师登录
router.post('/llogin', userHandler.llogin)
module.exports = router