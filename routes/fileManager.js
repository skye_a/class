// / 导入 express
const express = require("express");
// 创建路由对象
const router = express.Router();

// 导入用户信息的处理函数模块
const fileManager = require('../src/controller/fileManager.js')

// 展示学生上传的文件名
router.get('/AllFile', fileManager.queryAllFile)

// 展示老师上传的文件名
router.get('/lAllFile', fileManager.lqueryAllFile)

module.exports=router