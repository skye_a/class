// / 导入 express
const express = require("express");
// 创建路由对象
const router = express.Router();

// 导入用户信息的处理函数模块
const userinfo_handler = require('../src/controller/userinfo.js')

// 获取用户的基本信息[学生]
router.get('/stuInfo', userinfo_handler.getUserInfo)
// 获取用户的基本信息[老师]
router.get('/teaInfo', userinfo_handler.getlUserInfo)

// 向外共享路由对象
module.exports = router;
