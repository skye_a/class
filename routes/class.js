// / 导入 express
const express = require("express");
// 创建路由对象
const router = express.Router();

// 导入用户信息的处理函数模块
const classa = require('../src/controller/class.js')

// 老师创建课程
router.post('/createCourse', classa.createCourse)

// 查找当前老师创建的课程
router.get('/lqueryCourse', classa.lqueryCourse)


// 查找指定课程下的所有学生
router.get('/queryStu', classa.queryStu)

// /# 老师布置作业 [作业名] [截止时间] [课程id]
router.post('/createWork',classa.createWork)

//========================================================

// 学生加入课程
router.post('/intoCourse', classa.intoCourse)

// 查找当前学生加入的课程
router.get('/queryCourse', classa.queryCourse)

// 学生提交作业
router.post('/submit', classa.submit)

// 查询该课程下的所有作业以及自己的完成情况[课程id]
router.get('/queryWorkStu', classa.queryWorkStu)

// 查询该课程下的所有作业以及自己的完成情况[课程id]
router.get('/queryScore', classa.queryScore)

// ========================================================
// 查询该课程下的所有作业[课程id]
router.get('/queryWork', classa.queryWork)

// 查询该课程下的所有作业以及学生的完成情况[课程id]
router.get('/lqueryWorkStu', classa.lqueryWorkStu)

// 批改
router.get('/correct', classa.correct)


// 查询该作业学生的完成情况
router.get('/queryWorkDone', classa.queryWorkDone)

// 查询完成了该作业的学生
router.get('/queryStuWorkDone', classa.queryStuWorkDone)

// 查询未完成该作业的学生
router.get('/queryStuWorkNone', classa.queryStuWorkNone)

// 查询该课程下所有学生的详细成绩
router.get('/queryAllScore', classa.queryAllScore)


// 向外共享路由对象
module.exports = router;
