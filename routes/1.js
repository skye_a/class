// 在这里获取表单中关于作业的信息
var workName = fields.workName[0]; //作业名字
var course_id = fields.course_id[0]; //课程id
var deadline = fields.deadline[0]; //截止时间

// 首先获得课程名
var sql1 = "select courseName from course where id =?";
var courseName = "";
db.query(sql1, [course_id], function (err, results) {
	// 执行 SQL 语句失败
	if (err) {
		return res.sd(err.message);
	}

	if (results.length > 0) {
		courseName = results[0].courseName;
	}
	// 下面使用promise,使得异步任务不会出错
	const p = new Promise(function (resolve, reject) {
		var sdir = "./stuWork/" + courseName + "/" + workName;
		// 首先判断此次作业的文件夹是否创建
		isExit(sdir);
	});
	// 然后判断作业中老师上传作业的文件夹
	p.then(function (res) {
		// 接收resolve传来的数据，做些什么
		var sdir = "./stuWork/" + courseName + "/" + workName + "/target";
		isExit(sdir);
	});

	console.log(`创建[${courseName}]作业[${workName}]`);
	const sql = `select * from work where work_name=?`;

	db.query(sql, [workName], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		// 名被占用
		if (results.length > 0) {
			console.log("#", "数据库中已有相同作业");
			message += workName + "作业已存在 !,不再新建作业,只上传作业文件";
		} else {
			// 作业插入数据库
			const sql = `insert into work (work_name, createTime, deadline) values (?,now(),?)`;
			db.query(sql, [workName, deadline], (err, results) => {
				// 1. 执行 SQL 语句失败
				if (err) return res.sd(err);
				// 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
				if (results.affectedRows !== 1) {
					return res.sd("创建失败, 联系后端, 错误日志:\n", results);
				}

				// 插入关系
				const sql3 =
					"insert into work_course(work_id, course_id) value ((select id from work where work_name=?),?)";
				db.query(sql3, [workName, course_id], (err, results) => {
					// 1. 执行 SQL 语句失败
					if (err) return res.sd(err);
					// 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
					if (results.affectedRows !== 1) {
						return res.sd(
							"创建失败, 联系后端, 错误日志:\n",
							results
						);
					}
					message += "成功新建作业,接收文件";
				});
			});
		}

		// 处理上传的文件
		var sdir = "./stuWork/" + courseName + "/" + workName + "/target/";
		console.log("#", sdir);
		var inputFile = files["files"][0];
		// 重命名上传的文件
		fs.rename(
			inputFile.path,
			sdir + name + "-" + inputFile.originalFilename,
			function (err) {
				if (err) {
					console.log("rename error: " + err);
				} else {
					console.log("rename ok");
					res.sd(message, 0);
				}
			}
		);
	});
});
