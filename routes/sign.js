// / 导入 express
const express = require("express");
// 创建路由对象
const router = express.Router();
// 导入用户信息的处理函数模块
const signa = require('../src/controller/sign.js')

// 老师创建签到
router.post('/createSign', signa.createSign)

// 查询当前作业的未过期的签到信息
router.get('/querySign', signa.querySign)

// 查询当前作业的未过期的签到信息
router.get('/queryAllSign', signa.queryAllSign)

// 学生签到
router.post('/toSign', signa.toSign)


// 学生签到
router.get('/queryAllSignState', signa.queryAllSignState)

// 查询已经签到的学生
router.get('/querySignDone', signa.querySignDone)

// 查询未签到的学生
router.get('/querySignNone', signa.querySignNone)






module.exports= router