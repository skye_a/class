//1. 引入express
const express = require("express");

//2. 创建应用对象
const app = express();

// 导入解析请求体的包
var bodyParser = require("body-parser");

// 在注册路由之前,设置可以接收json数据和ulrencode格式的参数
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// 导入配置文件
const config = require("./src/utils/jwt.js");

// 解析 token 的中间件
const expressJWT = require("express-jwt");



// 使用 .unless({ path: [/^\/api\//] }) 指定哪些接口不需要进行 Token 的身份认证
app.use(
	// expressJWT({ secret: config.jwtSecretKey }).unless({ path: [/^\/api\/|^\/cs\//] })
	expressJWT({ secret: config.jwtSecretKey }).unless({
		path: [/^\/api\//],
	})
);

// 导入 cors 中间件
const cors = require("cors");
// 将 cors 注册为全局中间件
app.use(cors());

const joi = require("joi");

// 简化res.send函数
// 响应数据的中间件
app.use(function (req, res, next) {
	// status = 0 为成功； status = 1 为失败； 默认将 status 的值设置为 1，方便处理失败的情况
	res.sd = function (err, errCode = 1) {
		res.send({
			// 状态
			errCode,
			// 状态描述，判断 err 是 错误对象 还是 字符串
			message: err instanceof Error ? err.message : err,
		});
	};
	next();
});

// 全局错误中间件
app.use(function (err, req, res, next) {
	// token验证失败
	if (err.name === "UnauthorizedError")
		return res.send({
			errorCode: 1,
			message:
				"token验证失败! 检查token是否携带 或者过期(token有效期7天) ",
		});
	// 数据验证失败
	if (err instanceof joi.ValidationError) return res.send(err);
	// 未知错误
	res.send(err);
});

// 导入并注册用户路由模块
const userRouter = require("./routes/users.js");
app.use("/api", userRouter);

// 导入并使用 ==[用户信息]== 路由模块
const userinfoRouter = require("./routes/userinfo.js");
// 注意：以 /cs 开头的接口，都是有权限的接口，需要进行 Token 身份认证
app.use("/cs", userinfoRouter);

// 导入并使用 ==[课程]==路由模块
const classRouter = require("./routes/class.js");
app.use("/cs", classRouter);

// 导入并使用 ==[签到]==路由模块
const signRouter = require("./routes/sign.js");
app.use("/cs", signRouter);

// 导入上传文件模块
const uploadRouter = require("./routes/upload.js");
app.use("/file", uploadRouter);

// 导入管理上传文件模块
const fileManager = require("./routes/fileManager.js");
app.use("/file", fileManager);

// 导入老师上传文件模块

const luploadRouter = require("./routes/lupload.js");
app.use("/file", luploadRouter);

// 导入下载文件模块
const downloadRouter = require("./routes/download.js");
app.use("/file", downloadRouter);

//4. 监听端口启动服务
app.listen(10004, () => {
	console.log("starting...");
});
