

1.重启青龙容器

```java
docker restart 你的容器名  
1
```

容器名一般为qinglong或者ql等，不知道的跑一下下面命令可以看到

```java
docker ps -a
1
```

2.更新青龙

```java
docker exec -it qinglong ql update
1
```

3.更新青龙并编译

```java
docker exec -it qinglong ql restart
1
```

4.拉取自定义仓库，已Faker仓库为例

```java
docker exec -it qinglong ql repo https://ghproxy.com/https://github.com/shufflewzc/faker2.git "jd_|jx_|gua_|jddj_|getJDCookie" "activity|backUp" "^jd[^_]|USER|ZooFaker_Necklace.js|JDJRValidator_Pure|sign_graphics_validate"
1
```

5.拉取单个脚本，以Faker库的资产变更通知为例

```java
docker exec -it qinglong ql raw https://github.com/shufflewzc/faker2/blob/main/jd_bean_change_new.js
```







重启docker容器

docker restart nolanjdc

重启socks5

service ss5 restart

重启机器人

docker restart qqbot1

机器人静默挂机

nohup ./go-cqhttp &



机器人掉线问题

# 准备工作

**安装Screen**

```undefined
yum -y install screen
```



QQBot只是一个容器，其实还是go-cqhttp登录的QQ

## 1.杀进程

```perl
ps -A|grep go-cqhttp
```

没有运行的话就不会显示直接继续下一步就行

上面查出来的进程号替换掉xxx

```perl
kill -9 xxx
```

## 2.创建新的会话

这里的 go-cqhttp 可替换其他你能记住的

```perl
screen -S cqhttp
```

## 3.运行

创建完会进入一个新的页面 在这里运行go-cqhttp

```bash
cd /root/go-cqhttp&&./go-cqhttp
```

***\*运行好以后摁 Ctrl+a+d 这就就完成了\****

## 4.重新连接会话

```perl
screen -r cqhttp
```

**退出并且挂机 还是摁 Ctrl+a+d**

## 5.结束会话

```undefined
screen -S cqhttp -X quit
```













3、青龙面板 依赖库修复方案

（1）.一般出现这种错误：(缺依赖)
Error: Cannot find module ‘xx’
操作是：
docker exec -it ql(名称) pnpm install xx

docker exec -it qinglong1 pnpm install ds

faker3 库

```java
ql repo https://git.metauniverse-cn.com/https://github.com/shufflewzc/faker3.git "jd_|jx_|gua_|jddj_|jdCookie" "activity|backUp" "^jd[^_]|USER|function|utils|sendNotify|ZooFaker_Necklace.js|JDJRValidator_|sign_graphics_validate|ql|JDSignValidator|magic|depend" "main"
```













- # 2.搭建多容器

- ### 2.1多容器1

```bash
 docker run -dit \
   -v $PWD/ql2/config:/ql/config \
   -v $PWD/ql2/log:/ql/log \
   -v $PWD/ql2/db:/ql/db \
   -v $PWD/ql2/repo:/ql/repo \
   -v $PWD/ql2/raw:/ql/raw \
   -v $PWD/ql2/scripts:/ql/scripts \
   -v $PWD/ql2/jbot:/ql/jbot \
   -p 5711:5700 \
   --name qinglong2 \
   --hostname qinglong2 \
   --restart unless-stopped \
   whyour/qinglong:2.11.3
```

- ### 2.2多容器2

```bash
 docker run -dit \



   -v $PWD/ql2/config:/ql/config \



   -v $PWD/ql2/log:/ql/log \



   -v $PWD/ql2/db:/ql/db \



   -v $PWD/ql2/repo:/ql/repo \



   -v $PWD/ql2/raw:/ql/raw \



   -v $PWD/ql2/scripts:/ql/scripts \



   -v $PWD/ql2/jbot:/ql/jbot \



   -p 5720:5700 \



   --name qinglong2 \



   --hostname qinglong2 \



   --restart unless-stopped \



   whyour/qinglong:latest
```

- ### 2.3多容器3

```bash
 docker run -dit \



   -v $PWD/ql3/config:/ql/config \



   -v $PWD/ql3/log:/ql/log \



   -v $PWD/ql3/db:/ql/db \



   -v $PWD/ql3/repo:/ql/repo \



   -v $PWD/ql3/raw:/ql/raw \



   -v $PWD/ql3/scripts:/ql/scripts \



   -v $PWD/ql3/jbot:/ql/jbot \



   -p 5730:5700 \



   --name qinglong3 \



   --hostname qinglong3 \



   --restart unless-stopped \



   whyour/qinglong:latest
```

- ### 2.4多容器4

```bash
 docker run -dit \



   -v $PWD/ql4/config:/ql/config \



   -v $PWD/ql4/log:/ql/log \



   -v $PWD/ql4/db:/ql/db \



   -v $PWD/ql4/repo:/ql/repo \



   -v $PWD/ql4/raw:/ql/raw \



   -v $PWD/ql4/scripts:/ql/scripts \



   -v $PWD/ql4/jbot:/ql/jbot \



   -p 5740:5700 \



   --name qinglong4 \



   --hostname qinglong4 \



   --restart unless-stopped \



   whyour/qinglong:latest
```

- ### 2.5多容器5

```bash
 docker run -dit \



   -v $PWD/ql5/config:/ql/config \



   -v $PWD/ql5/log:/ql/log \



   -v $PWD/ql5/db:/ql/db \



   -v $PWD/ql5/repo:/ql/repo \



   -v $PWD/ql5/raw:/ql/raw \



   -v $PWD/ql5/scripts:/ql/scripts \



   -v $PWD/ql5/jbot:/ql/jbot \



   -p 5750:5700 \



   --name qinglong5 \



   --hostname qinglong5 \



   --restart unless-stopped \



   whyour/qinglong:latest
```

- ###  注：一般情况下，主容器的端口为5600，但对外使用会映射到5700，所以主容器的映射端口为5700:5600。

- ### 注：开启多容器后得修改相应的映射端口，例：主容器端口为5700:5600，其中映射端口为5700，被映射端口为5600。

- ### 注：搭建多容器1端口应改为5710:5700，其中映射端口为5710，被映射端口为主容器的5700，以此类推，搭建更多的容器时自行修改映射端口即可，如遇IP:端口打不开后台，请先去服务器内放行此端口。映射端口可自行修改。







# 解决Linux下Docker下载安装太慢

![img](https://csdnimg.cn/release/blogv2/dist/pc/img/original.png)

[减白](https://blog.csdn.net/weixin_45213446)![img](https://csdnimg.cn/release/blogv2/dist/pc/img/newCurrentTime2.png)于 2020-08-28 16:40:58 发布![img](https://csdnimg.cn/release/blogv2/dist/pc/img/articleReadEyes2.png)1120![img](https://csdnimg.cn/release/blogv2/dist/pc/img/tobarCollect2.png) 收藏 3

文章标签： [docker](https://so.csdn.net/so/search/s.do?q=docker&t=blog&o=vip&s=&l=&f=&viparticle=) [linux](https://so.csdn.net/so/search/s.do?q=linux&t=blog&o=vip&s=&l=&f=&viparticle=) [centos](https://so.csdn.net/so/search/s.do?q=centos&t=blog&o=vip&s=&l=&f=&viparticle=)

版权

卸载先前版本

```java
yum remove  docker \
            docker-client \
            docker-client-latest \
            docker-common \
            docker-latest \
            docker-latest-logrotate \
            docker-logrotate \
            docker-engine


12345678910
```

下载安装所需要的依赖
`yum install -y yum-utils device-mapper-persistent-data lvm2`

修改[Docker](https://so.csdn.net/so/search?q=Docker&spm=1001.2101.3001.7020)镜像下载地址
`yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo`

安装Docker
`yum install docker-ce docker-ce-cli containerd.io`



*#删除一个* 

docker rmi -f 镜像名/镜像ID

*# 先停止咱之前运行的 redis 容器*  

docker stop 容器名/容器ID
