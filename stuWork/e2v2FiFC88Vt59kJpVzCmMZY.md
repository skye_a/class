# 启动MySQL

systemctl start mysqld

重启： systemctl restart mysqld

停止： systemctl stopmysqld

# rpm安装之后查询自动生成的root用户密码

grep 'temporary password' /var/log/mysqld.log

# 修改用户密码

alter user 'root'@'localhost' identified by '1230';

# 设置密码校验等级

set global validate_password.policy=0;

# 设置密码长度

set global validate_password.length=4;

# 创建用户用于远程登陆

```mysql
create user 'root'@'%' identified with mysql_native_password by '1230';
```



安装完成之后登陆显示是root@localhost  代表只能在本机登陆数据库

这里创建一个root@%  代表在任意一台服务器上面都可以登陆MySQL

## 并且给用户分配权限

```mysql
grant all on *.* to 'root'@'%';
```

