# go-cqhttp监听配置



## go-cqhttp 配置

![image-20220813191025192](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220813191025192.png)

配置文件运行一次之后出现，打开编辑

![image-20220813191132106](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220813191132106.png)

## Python监听程序

```python
from flask import Flask, request
import requests
# 需要安装上面的两个模块
'''这是导入的另一个文件，下面会讲到'''

app = Flask(__name__)


class API:

    @staticmethod
    def send(msg):
        url = "http://192.168.1.14:8000/send_group_msg"
        '''go-cqhttp的API  下面是参数'''
        params = {
            "group_id": '935867462',
            "message": msg
        }
        requests.get(url, params=params)


@app.route('/', methods=["POST"])
def post_data():
    """下面的request.get_json().get......是用来获取关键字的值用的，关键字参考上面代码段的数据格式"""
    data = request.get_json()
    print(data)
    if data['post_type'] == 'message':
        # 这里是判断消息类型
        message = data['message']
        # 然后将收到的消息赋值给message
        print(message)
        msg = message
        # API.send(msg)  # 这里调用了上面发送到群聊里边的函数
    else:
        print("暂不处理")

    return "OK"


if __name__ == '__main__':
    # 此处的 host和 port对应上面 yml文件的设置
    app.run(host='0.0.0.0', port=5701)  # 保证和我们在配置里填的一致
```

登录好机器人，之后运行程序就可以看到机器人上报的所有数据。
