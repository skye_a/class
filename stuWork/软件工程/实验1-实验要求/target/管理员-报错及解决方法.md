# Spring

## 添加配置文件没有SpringConfig选项，

![image-20221009140238776](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009140238776.png)

首先创建依赖（添加Spring项目环境的依赖）

```xml
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-context</artifactId>
    <version>5.1.8.RELEASE</version>
</dependency>
```

然后刷新一下maven模块![img](file:///C:\Users\佩奇\AppData\Local\Temp\SGPicFaceTpBq\12720\52448912.png)

![image-20221009140341126](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009140341126.png)

## 配置文件找不到

```text
报错：
class path resource [applicationContext.xml] cannot be opened because it does not exist
```



![image-20221016165239697](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221016165239697.png)

但是好像会吧项目整个复制到该目录下

## 类HelloServlet不是Servlet

**原因：Tomacat 10 之后servlet依赖包名不是 javax.servlet，而是jakarta.servlet**

改为最新的dependency，这样javax就会自己变为jakarta。所以还是去maven Repository中搜我们需要的最新的dependency



先是servlet-api：

```XML
<!-- https://mvnrepository.com/artifact/org.apache.tomcat/tomcat-servlet-api -->
<dependency>
    <groupId>org.apache.tomcat</groupId>
    <artifactId>tomcat-servlet-api</artifactId>
    <version>10.0.4</version>
</dependency>
```

然后是jsp-api

> ```XML
> <!-- https://mvnrepository.com/artifact/org.apache.tomcat/tomcat-jsp-api -->
> <dependency>
>     <groupId>org.apache.tomcat</groupId>
>     <artifactId>tomcat-jsp-api</artifactId>
>     <version>10.0.4</version>
> </dependency>
> ```

**把这两个代码放到项目的pom中，覆盖掉原来的那两个dependency，等待maven更新就好。**

**然后最会还有一步就是你写的那个Java类中导入到javax包要手动改为jakarta**



