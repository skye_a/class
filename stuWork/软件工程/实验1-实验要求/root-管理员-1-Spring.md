# Spring创建项目步骤

![image-20221009134127188](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009134127188.png)

### ①导入Spring开发的基本包坐标

创建maven项目，在pom.xml中添加如下代码

```xml
    <dependencies>
        <dependency>
            <groupId>org.springframework</groupId> 
            <artifactId>spring-context</artifactId>  导的包
            <version>4.3.11.RELEASE</version>  版本
        </dependency>
    </dependencies>
```

### ②编写接口和实现类

![image-20221009140205894](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009140205894.png)

### ③创建Spring核心配置文件

![image-20221009140238776](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009140238776.png)

如果没有SpringConfig选项，

首先创建依赖

```xml
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-context</artifactId>
    <version>5.1.8.RELEASE</version>
</dependency>
```

然后刷新一下maven模块![img](file:///C:\Users\佩奇\AppData\Local\Temp\SGPicFaceTpBq\12720\52448912.png)

![image-20221009140341126](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009140341126.png)

### ④在配置文件中配置实现类

在配置文件中，配置全包名以及对应的ID

![image-20221009141611959](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009141611959.png)

### ⑤使用Spring的API获得Bean实例

```java
ApplicationContext app = new ClassPathXmlApplicationContext("ApplicationContext.xml");
FirstDro test=(FirstDro)app.getBean("userDao");
test.run();
```

# Spring标签配置

![image-20221009174803468](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009174803468.png)

## scope

![image-20221009142253102](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009142253102.png)

### scope="singleton"

- 当加载配置文件的时候，bean就会被创建出来！

- 每次getBean时，得到的Bean是同一个！

### scope="prototype"

- 当getBean时创建Bean。
- 每次getBean时，都创建一个新的Bean



## 配置初始化和销毁方法

### init-method

指定一个方法为初始化方法，当对象创建之后会首先运行这个方法！

### destroy-method

当容器关闭时，会执行这个方法



## Bean实例化3种方式

### 无参构造实例化对象

![image-20221009141611959](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009141611959.png)

### 工厂静态方法实例化

```xml
<bean id="userDao" class="com.lwb.B_factory.StaticFctory" factory-method="getDao"/>
```

此处的 factory-method="getDao"  对应的getDao方法，返回值是最终对应此处ID的Bean

### 工厂实例方法实例化

```xml
    <bean id="factory" class="com.lwb.B_factory.DynamicFactory"/>
    <bean id="userDao" factory-bean="factory" factory-method="getDao"/>
```

先创建一个工厂对象，然后使用工厂对象来进行实例化

## 依赖注入

### 普通方式

当有另一个service层（另一个类）的时候，也需要在xml文件中进行配置

![image-20221009161844610](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009161844610.png)

​     在容器外部分别获得userService和userDao的实例对象 然后在程序中进行结合
​                                                     ↓
​                                      通过依赖注入进行优化
​        在容器内部将Dao设置到Service内部，使程序最终使用的是service

![image-20221009161851131](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009161851131.png)

```xml
        <bean id="userService" class="com.lwb.Service.impl.userServiceImpl">
                <property name="userDao" ref="userDao"/>
        </bean>
```

在xml中配置Dao依赖注入        将userDao注入到userService中  

使用userService就不用再getBean()  实例化Dao对象了  

### set方法注入-p命名空间注入（了解）

![image-20221009164217971](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009164217971.png)

​																			↓

```xml
<bean id="userService" class="com.lwb.Service.impl.userServiceImpl">
        <property name="userDao" ref="userDao"/>
</bean>
```

​																			↓   可以改写为

```xml
<bean id="userService" class="com.lwb.Service.impl.userServiceImpl" p:userDao-ref="userDao"/>
```

### 有参构造注入

```xml
<bean id="userService" class="com.lwb.Service.impl.userServiceImpl">
        <constructor-arg name="userDao" ref="userDao"/>
</bean>
```

```java
//有参构造方法
public userServiceImpl(userDaoImpl userDao) {
    this.userDao=userDao;
}
```

## bean的依赖注入的数据类型

- 普通数据类型
- 集合数据类型
- 引用数据类型

### set方法注入（最常用）

#### 普通数据类型

```java
private String username;
private int age;

public void setUsername(String username) {
    this.username = username;
}

public void setAge(int age) {
    this.age = age;
}
```

通过配置文件，将username和age进行赋值



```xml
<bean id="userDao" class="com.lwb.Dao.impl.userDaoImpl">
        <property name="username" value="liWenBin"/>
        <property name="age" value="18"/>
</bean>
```



#### 集合数据类型

list集合

```java
private List<String> strList;
```

```xml
<property name="strList">
        <list>
                <value>List1</value>
                <value>List2</value>
                <value>List3</value>
        </list>
</property>
```

map集合

```java
private Map<String, SP> userMap;
```

```xml
<property name="userMap">
        <map>
                <entry key="liwenbin" value-ref="SP"/>
                <entry key="liangjinfeng" value-ref="SP2"/>
        </map>
</property>
```

properties

```java
private Properties properties;
```

```xml
<property name="properties">
        <props>
                <prop key="pros1">haha</prop>
                <prop key="pros2">heihei</prop>
        </props>
</property>
```

## 分模块开发

配置文件数据量很大的时候，可以分开成为多个配置文件

![image-20221009174233607](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009174233607.png)

但是  总的配置文件只有1个  需要将其他配置文件引入主配置文件

![image-20221009174455569](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221009174455569.png)

# Spring相关API

## ApplicationContext

```java

    ApplicationContext app = new ClassPathXmlApplicationContext("ApplicationContext.xml");
    userDao test=(userDao)app.getBean("userDao");
    test.run();
    
```

### ClassPathXmlApplicationContext

从类加载文件目录加载配置文件

### FileSystemXmlApplicationContext

从磁盘任意位置加载配置文件

### AnnotationConfigApplicationContext

当使用注解配置容器对象时，需要使用此类来创建Spring容器，它用来读取注解。

## getBean()方法

### 源码

![image-20221010110707452](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221010110707452.png)

![image-20221010112725151](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221010112725151.png)

```java
userService test2=(userService)app2.getBean("userService")//id
userService test2=app2.getBean(com.lwb.Service.userService.class);//class
```

# Spring配置数据源（连接池）

## 数据源（连接池）的作用

- 数据源是伪提高程序性能而出现的

- 实现实例化数据源，初始化部分连接资源

- 使用连接资源时从数据源中获取

- 使用完毕之后将连接资源归还给数据源

  

  常见的数据源有：DBCP ==C3P0== BoneCP ==Druid==

## 数据源的开发步骤

- 导入数据源的坐标和数据库驱动坐标
- 创建数据源对象
- 设置数据源的金恩连接数据
- 使用数据源获取资源和归还连接资源



### pom.xml中导入数据源的坐标和数据库驱动坐标

```xml
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.30</version>
</dependency>
<dependency>
    <groupId>c3p0</groupId>
    <artifactId>c3p0</artifactId>
    <version>0.9.1.2</version>
</dependency>
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid</artifactId>
    <version>1.1.10</version>
</dependency>
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.12</version>
</dependency>
```

这里爆红，点旁边maven，刷新依赖，联网下载没有的依赖。

### 创建数据源对象

#### 手动测试c3p0数据源

```java
public class c3p0Test {
    @Test
    //测试手动创建c3p0数据源  ==》运行成功
    public void test1() throws PropertyVetoException, SQLException {
        ComboPooledDataSource dataSource=new ComboPooledDataSource();
        dataSource.setDriverClass("com.mysql.jdbc.Driver");//设置驱动
        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/temp");  //数据局地址
        dataSource.setUser("root");
        dataSource.setPassword("1230");
        Connection connection= dataSource.getConnection();
        System.out.println(connection);// 打印出地址  不为空则连接成功
        connection.close();//关闭连接   将数据源归还

    }
}
```

最后得到connection的地址

```text
com.mchange.v2.c3p0.impl.NewProxyConnection@59fd97a8
```



#### 手动测试druid数据源

```java
@Test
//测试手动创建Druid数据源  ==》运行成功
public void test2() throws  SQLException {
    DruidDataSource dataSource=new DruidDataSource();
    dataSource.setDriverClassName("com.mysql.jdbc.Driver");//设置驱动
    dataSource.setUrl("jdbc:mysql://localhost:3306/temp");  //数据局地址
    dataSource.setUsername("root");
    dataSource.setPassword("1230");
    Connection connection= dataSource.getConnection();
    System.out.println(connection);// 打印出地址  不为空则连接成功
    connection.close();//关闭连接   将数据源归还

}
```

## 抽取配置文件（![img](file:///C:\Users\佩奇\AppData\Local\Temp\SGPicFaceTpBq\2964\4D62E49F.png))

为了使这些字符串数据配置跟数据源耦合，要进行抽取配置文件

![image-20221010180547543](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221010180547543.png)

### 首先创建配置文件

![image-20221010183950123](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221010183950123.png)

### 然后在配置文件中规定好字符串

```text
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/temp
jdbc.username=root
jdbc.password=1230
```

### 最后在程序中引入配置文件获取数据

```java
@Test
//测试手动创建c3p0数据源 (读取配置文件的模式)   ==》 运行成功
public void test3() throws PropertyVetoException, SQLException {
    ResourceBundle rb=ResourceBundle.getBundle("jdbc");//首先读取配置文件==》 这里只写名字 不写.properties后缀
    String driver=rb.getString("jdbc.driver");
    String url=rb.getString("jdbc.url");
    String username=rb.getString("jdbc.username");
    String password=rb.getString("jdbc.password");// 获取配置文件中定义的数据

    ComboPooledDataSource dataSource=new ComboPooledDataSource();
    dataSource.setDriverClass(driver);
    dataSource.setJdbcUrl(url);
    dataSource.setUser(username);
    dataSource.setPassword(password);// 使用独取出来的数据

    Connection connection=dataSource.getConnection();//获取连接对象
    System.out.println(connection);
    connection.close();//归还连接对象资源
}
```

## Spring配置数据源（![img](file:///C:\Users\佩奇\AppData\Local\Temp\SGPicFaceTpBq\2964\4D64F02E.png))  

![image-20221010184504154](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221010184504154.png)

因为读取配置时，程序中有很多set方法，可以在xml中配置，将数据源的创建权交给Spring容器，跟依赖注入一样的道理

### 步骤又回到了创建Spring程序

![image-20221010185109489](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221010185109489.png)

### pro.xml中导Spring对应的坐标（spring-context）

![image-20221010185933519](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221010185933519.png)

### 在resources中创建配置文件

class=如下

![image-20221010190303226](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221010190303226.png)

#### set get 方法是谁的  就要需要注入  就导入谁

```xml
    <bean id="datasourses" class="com.mchange.v2.c3p0.ComboPooledDataSource">
<!--set get方法是谁的  这里就导入谁-->
<!--            这里name要填set方法名去掉set之后首字母小写-->
        <property name="driverClass" value="com.mysql.jdbc.Driver"/>
        <property name="jdbcUrl" value="jdbc:mysql://localhost:3306/temp"/>
        <property name="user" value="root"/>
        <property name="password" value="1230"/>
    </bean>
```

上面程序没有引入jdbc的配置文件，将数据写死了

引入jdbc配置文件的在下一节

### 在程序中使用

```java
@Test
//测试Spring容器创建c3p0数据源   ==》 运行成功
public void test4() throws PropertyVetoException, SQLException {
    //读取配置文件
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("ApplicationContext.xml");

    //获取bean
    DataSource ds = applicationContext.getBean(DataSource.class);
    //ComboPooledDataSource ds = applicationContext.getBean(ComboPooledDataSource.class);//或者这样写
    //获取连接对象
    Connection connection= ds.getConnection();
    System.out.println(connection);
    connection.close();//归还连接对象资源
}
```

## Spring配置数据源引入jdbc配置文件

![image-20221010192957279](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221010192957279.png)

### ①引入context命名空间和约束路径

![image-20221010193353544](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221010193353544.png)

```xml
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                            http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">
```

### ②ApplicationContext引入properties配置文件

```xml
<!--    引入properties配置文件-->
    <context:property-placeholder location="classpath:jdbc.properties"/>
    <bean id="datasourses" class="com.mchange.v2.c3p0.ComboPooledDataSource">
        <!--set get方法是谁的  这里就导入谁-->
        <!--            这里name要填set方法名去掉set之后首字母小写-->
        <property name="driverClass" value="${jdbc.driver}"/>
        <property name="jdbcUrl" value="${jdbc.url}"/>
        <property name="user" value="${jdbc.username}"/>
        <property name="password" value="${jdbc.password}"/>
    </bean>
```

### ③在程序中使用

```java
@Test
//测试Spring容器创建c3p0数据源   ==》 运行成功
public void test4() throws PropertyVetoException, SQLException {
    //读取配置文件
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("ApplicationContext.xml");

    //获取bean
    DataSource ds = applicationContext.getBean(DataSource.class);
    //ComboPooledDataSource ds = applicationContext.getBean(ComboPooledDataSource.class);//或者这样写
    //获取连接对象
    Connection connection= ds.getConnection();
    System.out.println(connection);
    connection.close();//归还连接对象资源
}
```

# Spring原始注解开发

Spring是轻代码，重配置的框架。

## Spring原始注解步骤

Spring原始注解，主要是替代<Bean>标签的配置  -->配置工作量减少

![image-20221011165019811](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221011165019811.png)

### ①在applicationContext.xml中删除bean标签配置

### ②引入context并且配置组件扫描

```http
xmlns:context="http://www.springframework.org/schema/context"

http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
```

![image-20221011181726088](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221011181726088.png)

```text
    组件扫描
        作用是告诉Spring在哪里写了注解，需要去处理配置

    这样配置了之后，会扫描com.lwb1下的包及其子包  只要有注解 都会被识别
```



```xml
<context:component-scan base-package="com.lwb1"/>
```

### ③在程序中进行注解

#### 引入全包名以及id 用@Component("userDao")代替

![image-20221011181849412](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221011181849412.png)

#### service类里边注入Dao类用@Qualifier("userDao")

![image-20221011182118610](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221011182118610.png)

之后 即可在程序中正常获取bean了

```java
public class UserController {
    public static void main(String[] args) {
        ApplicationContext app= new ClassPathXmlApplicationContext("applicationContext.xml");
        userService userService= (com.lwb1.service.userService) app.getBean("userService");
        userService.save();//正常运行
    }
}
```

## 使用更合适的注解

![image-20221015133659497](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221015133659497.png)

![image-20221015133851713](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221015133851713.png)

## 注意

![image-20221015135154546](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221015135154546.png)

## 注入普通数据@Value

![image-20221015142101121](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221015142101121.png)

# Spring新注解开发

有些配置，原始注解无法替代，比如，加载外部配置文件，组件扫描

![image-20221015144410493](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221015144410493.png)

## 新注解

![image-20221015144623889](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221015144623889.png)

## 核心配置类（代替ApplicationContext.xml）

创建一个类config.SpringConfigApplicationuration作为Spring核心配置类

```java
//标志该类是Spring核心配置类（能够代替ApplicationContext.xml配置文件的类）
@Configuration
//配置文件中Spring组件扫描标签  转换为注解
//    <context:component-scan base-package="com.lwb1"/>
@ComponentScan("com.lwb1")

//分开的配置文件类需要引入 对应配置文件中的import标签
@Import(DataSourceConfiguration.class)
//需要引入多个括号内({xxx.class,yyy.class})
public class SpringConfiguration {


}
```

如果配置文件数据很多，可以分模块开发，写多个类，引入主配置文件。

```java
//加载外部的properties文件
//    <context:property-placeholder location="classpath:jdbc.properties"/>
@PropertySource("classpath:jdbc.properties")
public class DataSourceConfiguration {
    //将配置文件中的值注入进去
    @Value("${jdbc.driver}")
    private String jdbc_driver;
    @Value("${jdbc.url")
    private String jdbc_Url;
    @Value("${jdbc.username}")
    private String jdbc_username;
    @Value("${jdbc.password")
    private String jdbc_password;

    @Bean("dataSource")// Spring会将当前方法的返回值以指定名称存到Spring容器中
    public DataSource getDataSource() throws PropertyVetoException {
        ComboPooledDataSource dataSource=new ComboPooledDataSource();
        dataSource.setDriverClass(jdbc_driver);
        dataSource.setDriverClass(jdbc_Url);
        dataSource.setDriverClass(jdbc_username);
        dataSource.setDriverClass(jdbc_password);
        return dataSource;
    }
}
```

在web层，需要用配置类要以下面方法

```java
public class UserController {
    public static void main(String[] args) {
        //这是加载配置文件
        //ApplicationContext app= new ClassPathXmlApplicationContext("applicationContext.xml");

        //当写了配置类  需要用以下方法引入配置
        ApplicationContext app=new AnnotationConfigApplicationContext(SpringConfiguration.class);
        userService userService= (com.lwb1.service.userService) app.getBean("userService");//这里用id，在Spring容器中寻找匹配的Bean
        userService.save();
    }


}
```

# Spring整合Junit

因为之后每个测试类都会有这两行代码来获取容器以及Spring容器中的Bean

![image-20221015165507981](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221015165507981.png)

所以可以让SprintJunit来负责创建Spring容器，并将配置文件/类的名称告诉它，将需要测试的Bean直接在测试类中进行注入

## 步骤

### ①导入Spring集成Junit的坐标

导入Spring-test跟junit依赖

```xml
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.13.2</version>
    <scope>test</scope>
</dependency>
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-test</artifactId>
    <version>5.3.23</version>
</dependency>
```

### ②使用@RunWith()替换原来的运行器

![image-20221015173612386](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221015173612386.png)

```java
@RunWith(SpringJUnit4ClassRunner.class)
```

### ③使用@ContextConfiguration指定配置文件或配置类

![image-20221015173839511](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221015173839511.png)

```java
@ContextConfiguration("classpath:applicationContext.xml")//使用配置文件
@ContextConfiguration(classes = {SpringConfiguration.class})//使用配置类
```

### ④使用@Autowried注入需要测试的对象

![image-20221015173902195](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221015173902195.png)

### ⑤创建测试方法进行测试

![image-20221015175701724](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221015175701724.png)

要测试什么就直接注入!

# Spring集成Web环境

## 基本环境搭建

首先创建空项目，然后添加模块   -->   

![image-20221018172237986](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221018172237986.png)



创建UserDao和UserService

![image-20221016155309790](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221016155309790.png)





```java
// Service程序内容
public class UserServiceImpl implements UserService {
    private UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void SaveService() {
        userDao.saveDao();                                                    
    }
}
```

所需依赖

```xml
<!--    引入Servlet程序所需要的依赖-->
    <dependencies>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>4.0.1</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet.jsp</groupId>
            <artifactId>javax.servlet.jsp-api</artifactId>
            <version>2.3.3</version>
        </dependency>
<!--        下面是Spring所需的依赖-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>5.3.23</version>
        </dependency>
    </dependencies>
```

applicationContext.xml中配置

```xml
<bean id="UserDao" class="com.lwb1.Dao.impl.UserDaoImpl"/>
<bean id="UserService" class="com.lwb1.Service.Impl.UserServiceImpl">
    <property name="userDao" ref="UserDao"/>
</bean>
```

创建Servlet程序

```java
public class UserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService userService= (UserService) app.getBean("UserService");
        userService.SaveService();
    }
//    public static void main(String[] args) {
//        ApplicationContext app=new ClassPathXmlApplicationContext("classpath*:applicationContext.xml");
//
//        UserService userService= (UserService) app.getBean("UserService");
//        userService.SaveService();
//    }
}
```

## 弊端分析

①在Servlet程序中，要加载applicationContext.xml配置文件，有多个程序就要加载多次，就要创建多个上下文对象，浪费内存，可以将获取上下文对象放在servletContext监听器中，然后放在最大的域ServletContext域中（application域）在任何位置都可以调用上下文对象。

②resources中的配置文件名字写死了，如果要修改名字，需要修改多处，可以web.xml全局参数优化

③在application域中存入上下文对象并取名为app（上下文对象）。getAttribute（）取的时候，需要记住app这个名字，可以写个优化函数，让我们不用记住application域中存的数据的名字。

![image-20221018180500503](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221018180500503.png)

## 上下文对象配合监听器（了解）

为了解决上面弊端

### ①创建Listener.ContextLoaderListener用来作为监听器

```java
public class ContextLoaderListener implements ServletContextListener {//监听什么就实现对应的接口
    //重写初始化方法
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("开始初始化");
        //创建ServletContext对象
        ServletContext servletContext=sce.getServletContext();
        //读取web.xml中的全局参数
        String config = servletContext.getInitParameter("contextConfig");

        //创建上下文对象
        ApplicationContext app=new ClassPathXmlApplicationContext(config);

        //将app存在application域中
        servletContext.setAttribute("app",app);
    }
}
```

### ②创建ApplicationUtils类并写返回域中数据的静态方法

```java
//获取application域中的app（配置文件的上下文对象）
public class WebApplicationUtils {
    public static Object getApplicationName(ServletContext servletContext){//传入域对象
        return servletContext.getAttribute("app");  //在传入的application域对象中获取app 然后返回
    }

}
```

### ③web.xml中配置监听器和全局初始化参数

```xml
<!--  配置监听器-->
<listener>
  <listener-class>com.lwb1.Listener.ContextLoaderListener</listener-class>
</listener>
<!--  全局初始化参数  将resources中的配置文件的名字存入，变量名为contextConfig-->
  <context-param>
    <param-name>contextConfig</param-name>
    <param-value>applicationContext.xml</param-value>
  </context-param>
```

### ④最终改善获取上下文对象的方法

```java
public class UserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //原来的获取上下文对象的方法
        //ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
        // 将上下文对象通过监听器，放入application域中，通过以下方法获取
        ServletContext servletContext= req.getServletContext();

        //原本获取application中app的方法
        //ApplicationContext app= (ApplicationContext) servletContext.getAttribute("app");
        //通过WebApplicationUtils函数优化之后
        ApplicationContext app= (ApplicationContext) WebApplicationUtils.getApplicationName(servletContext);

        UserService userService= (UserService) app.getBean("UserService");
        userService.SaveService();
    }
}
```

## Spring提供的获取上下文对象的工具

![image-20221018192915813](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20221018192915813.png)

## ①在pom.xml中导入坐标

```xml
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-web</artifactId>
  <version>5.0.5.RELEASE</version>
</dependency>
```

## ②在web.xml中配置提供的监听器和全局参数

```xml
<!--  全局初始化参数  将resources中的配置文件的名字存入，变量名为contextConfig-->
  <context-param>
    <param-name>contextConfig</param-name>
    <param-value>classpath:applicationContext.xml</param-value>
  </context-param>

  <!--  配置监听器-->
  <listener>
    <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
  </listener>

```

## ③使用自带的监听器工具

```java
//通过spring-web提供的获取app的方法
WebApplicationContext app = WebApplicationContextUtils.getWebApplicationContext(servletContext);

UserService userService= (UserService) app.getBean("UserService");
userService.SaveService();
```