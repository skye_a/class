# Mysql基础

```mysql
# 数据库的备份
create table wahaha as
    select * from blogs.article;

# 创建存储过程
create procedure select_all_article()
begin
    select * from wahaha;
end;

# 调用存储过程
call select_all_article();
```