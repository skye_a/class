# 数据库语言类型

## 数据操纵语言 DML

关键字：INSERT、UPDATE、DELETE。

适用范围：对数据库中的数据进行一些简单操作，如insert,delete,update等.

## 数据定义语言 DDL

关键字：CREATE，DROP，ALTER。

适用范围：对数据库中的某些对象(例如，database,table)进行管理，如Create,Alter和Drop.

## 数据控制语言 DCL

关键字：GRANT、REVOKE。

是用来设置或更改数据库用户或角色权限的语句，并控制数据库操纵事务发生的时间及效果，对数据库实行监视等；

## 数据查询语言 DQL

关键字：SELECT ... FROM ... WHERE。

# ACID模型

ACID，是指在数据库管理系统（DBMS）中，事务(transaction)所具有的四个特性：[原子性](https://so.csdn.net/so/search?q=原子性&spm=1001.2101.3001.7020)（Atomicity）、一致性（Consistency）、隔离性（Isolation，又称独立性）、持久性（Durability）。

- 原子性：一个事务(transaction)中的所有操作，要么全部完成，要么全部不完成，不会结束在中间某个环节。事务在执行过程中发生错误，会被回滚（Rollback）到事务开始前的状态，就像这个事务从来没有执行过一样。
- 一致性：在事务开始之前和事务结束以后，数据库的完整性限制没有被破坏。
- 隔离性：当两个或者多个事务并发访问（此处访问指查询和修改的操作）数据库的同一数据时所表现出的相互关系。事务隔离分为不同级别，包括读未提交(Read uncommitted)、读提交（read committed）、可重复读（repeatable read）和串行化（Serializable）。
- 持久性：在事务完成以后，该事务对数据库所作的更改便持久地保存在数据库之中，并且是完全的。

# MySQL的体系结构

## 连接层  

连接层又称为客户端连接器（Client Connectors）：提供与MySQL服务器建立的支持。
目前几乎支持所有主流的服务端编程技术，主要完成一些类似于连接处理、授权认证、及相关的安全方案。

在该层上引入了线程池的概念，为通过认证安全接入的客户端提供线程。同样在该层上可以实现基于SSL的安全连接，并且在这里服务器也会为接入的客户验证他所具备的权限。
注意：连接用户权限修改后，不会立即生效，就是因为在这里将用户认证后的权限信息保存了下来，需要用户断开并重新连接，重新进行校验，修改后的权限才会生效。

## 服务层 

服务层是MySQL Server的核心，主要包含系统管理和控制工具、SQL接口、解析器、查询优化器。

实际上，连接层中的连接认证，校验流程，也可以归属在服务层中。
数据库连接成功后，在服务层完成大多数核心功能，如Sql接口，查询缓存，Sql分析和优化，以及一些内置函数的执行。
它的主要流程是这样的：

第一步，查看当前查询是否存在缓存，有则直接返回，没有则进行服务器解析，并生成对应的内部解析树。
第二步，完成相应的优化，确定表的查询顺序，是否使用索引
第三步，生成对应的操作。

## 引擎层

存储引擎负责MySQL中数据的存储与提取，与底层系统文件进行交互。MySQL存储引擎是插件式的，不同的存储引擎具备不同的功能，可以根据自己的需要进行选择，最常见的是MyISAM和InnoDB。

## 存储层 

存储层负责将数据库的数据和日志存储在文件系统之上，并完成与存储引擎的交互，是文件的物理存储层。主要包含日志文件（redolog,undo.log），数据文件，索引文件，二进制日志，错误日志，配置文件，pid 文件，socket 文件等。



![image-20220723185352408](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220723185352408.png)



# MySQL运行机制

![image-20220723185929508](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220723185929508.png)

- 建立连接（Connectors&Connection Pool），通过客户端/服务器通信协议与MySQL建立连接。MySQL 客户端与服务端的通信方式是 “ 半双工通信 ”。

- 查询缓存（Cache&Buffer），如上图所示，存在则直接返回，不存在则进行查询，不过需要注意的是，它默认是关闭的，并且，在8.0以后版本，不再支持查询缓存，因为它需要SQL完全相同才能匹配缓存，命中率极低，如果表进行DDL或者DML操作，缓存就会被清空。

- 解析器（Parser），将客户端发送的SQL进行语法解析，生成"解析树"。预处理器根据一些MySQL规则进一步检查“解析树”是否合法，例如这里将检查数据表和数据列是否存在，还会解析名字和别名，看看它们是否有歧义，最后生成新的“解析树”。

- 优化器（Optimizer），根据“解析树”生成最优的执行计划。MySQL使用很多优化策略生成最优的执行计划，分为两类：静态优化（编译时优化）、动态优化（运行时优化）。

- 查询执行引擎负责执行 SQL 语句，得到查询结果并返回给客户端。若开启用查询缓存，这时会将SQL 语句和结果完整地保存到查询缓存（Cache&Buffer）中，以后如果有相同的 SQL 语句执行则直接返回结果。

# 存储引擎

show engines; 命令可以查看当前数据库可以支持的存储引擎

## innoDB

innoDB是一种兼顾高可靠性和高性能的通用存储引擎。（MySQL默认的存储引擎）

### 特点：

DML操作遵循ACID模型，支持<font color='yellow'>事务</font>。

支持<font color='yellow'>外键</font>FOREIGN KEY约束，保证数据的完整性和正确性。

<font color='yellow'>行级锁</font>，提高并发访问性能。

### 文件

每一个innoDB引擎的表都会对应一个xxx.ibd的表空间文件，存储该表的表结构，数据和索引。<font color='yellow'>（是i b d ）</font>

ibd文件（二进制文件）可以在cmd中用idb2sdi xxx.ibd命令查看  返回的是一段json

### 逻辑存储结构

![image-20220723163730613](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220723163730613.png)



## MyISAM

MyISAM是MySQL早期的默认存储引擎。

### 特点

不支持事务，不支持外键。

支持<font color='yellow'>表锁</font>，不支持行锁。

访问速度块。

### 文件

在磁盘中，MyISAM存储的表主要有三个文件

xxx.MYD   -->表中存放的数据

xxx.MYI  --> 存储表的索引

xxx.sdi    --> 存储表结构文件

sdi文件可以以文本方式打开，是一段json

## Memory

Memory引擎的表数据是存放在内存当中的。由于受到硬件问题，或者断电问题的影响，只能将这些表作为临时表或者缓存使用。

### 特点

内存存放。

hash索引（默认）

### 文件

xxx.sid  -->存放表结构信息

## 存储引擎之间的区别

![image-20220723170221759](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220723170221759.png)

## 存储引擎的选择

存储引擎没有好坏之分，我们只要在合适的业务场景下，选择合适的存储引擎即可。

根据业务系统的特点和需求进行选择存储引擎。

![image-20220723170441359](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220723170441359.png)

# 索引

索引是帮助MySQL<font color='yellow'>高效获取数据</font>的<font color='yellow'>有序</font>的数据结构。

select查询时：

![image-20220725150745140](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725150745140.png)

![image-20220725150806045](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725150806045.png)

（这里二叉树只是举例说明索引，并不是索引真实结构）

## 索引优缺点

![image-20220725151027649](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725151027649.png)

![image-20220725151046164](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725151046164.png)

## 索引结构

![image-20220725151259554](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725151259554.png)

## 不同的存储引擎对索引的支持情况

![image-20220725151356908](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725151356908.png)

<font color='yellow'>平常所说的索引，没有特殊说明，一般指的就是B+tree索引</font>

## B+树

### 二叉树

![image-20220725151949226](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725151949226.png)

### B树

多路平衡查找树

![image-20220725152323381](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725152323381.png)

原理：

插入数据时，中间元素会向上分裂。

5阶B树举例：

![image-20220725153240522](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725153240522.png)

插入1200 后 若插在899后面，不满足5阶了（4个人数据，5个指针够了）所以此时345作为中间元素，345向上分裂。

![image-20220725153408183](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725153408183.png)

...

![image-20220725153533897](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725153533897.png)

插入1000

![image-20220725153559343](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725153559343.png)

演示完毕

### B+树

4阶B+树为例：

![image-20220725154352717](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725154352717.png)

#### 特点

<font color='yellow'>B+树中，所有非叶子节点都会出现在叶子结点中。</font>

<font color='yellow'>非叶子节点是索引，叶子节点就是存储的数据。</font>

<font color='yellow'>叶子结点形成了一个单向链表。每个节点通过指针指向下一节点。</font>

### MySQL中的B+树

![image-20220725154853404](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725154853404.png)

每一个节点都存放在唉一个页当中，一各页大小固定是16k，所以：

每个节点存放的key和指针是有限的。

## Hash

![image-20220725173907258](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725173907258.png)

### 特点

![image-20220725173927501](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725173927501.png)

### 存储引擎支持

在MySQL中，支持hash索引的是Memory存储引擎。

innoDB具有自适应hash功能。

hash索引是存储引擎根据B+tree索引在指定条件下自动构成的。

## 索引的分类

![image-20220725175525700](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725175525700.png)

在innoDB引擎中，根据索引的形式，有可以分为以下两种：
![image-20220725180059559](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725180059559.png)

### 聚集索引选取规则

- 如果存在主键，主键索引就是聚集索引。
- 如果没有主键，将使用第一个唯一索引作为聚集索引。
- 如果没有主键，没有合适的唯一索引，则innoDB会自动生成一个rowid作为隐藏的聚集索引。

### 区别

![image-20220725181527611](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725181527611.png)

## 索引查询步骤（回表查询）

![image-20220725181854900](C:\Users\佩奇\AppData\Roaming\Typora\typora-user-images\image-20220725181854900.png)

根据name字段进项查询

→name不是主键

→在name字段对应的二级索引里查询到Arm的ID

→用聚集索引查询到ID为10的所有数据

→down

## 索引语法

- 创建索引

```mysql
create [unique|fulltext] index index_name on table_name (index_col_name , ...)
```



如果一个索引关联了一个字段，则成这个索引为单列索引，否则为联合索引 / 组合索引。

- 查看索引



- 删除索引

# 面试

==1.==为什么innodb存储引擎选择使用B+tree索引结构？

- 相对于二叉树，层级更少，搜索效率更高。（因为二叉树在插入顺序数据的时候是一条单链表）

- 对于B-tree，无论是叶子结点还是非叶子节点，都会保存数据，这样导致一页中存储的键值减少，指针也减少，若要保存大量数据，只能增加树的高度，导致性能降低。（逻辑存储结构）

- 相对hash索引，B+tree范围匹配以及排序操作。（hash只能等值匹配，不能范围搜索，B+tree因为叶子结点之间的双向链表指针，所以它能支持）

