# 我的框架开发文档

# API调用 帮助文档

### HTTP、WebSocket调用地址

**地址格式：**`(http | ws)://IP 或 域名:端口号/`

> 端口可以随意设置，但需要自行开放安全组

##### 例子：

**本地：**`(http | ws)://127.0.0.1:8863/`
**内网：**`(http | ws)://192.168.0.167:8863/`
**外网：**`(http | ws)://xpw.apifox.cn:8863/`

> 若外网调用，请确保您的网络拥有公网IP，否则无法使用
> 服务器：进入供应商控制台查看，找不到或有问题请联系您的供应商客服
> 本 地：需联系您当地网络供应商开通公网IP
> 提示：只能提示这么多，此类问题恕不解答

------

### 调用方式为 GET 或 POST 或 WebSocket

> 除 GET 外，必须使用 JSON

| 参数名     | 说明         | 类型   | 必填 | 备注                            |
| ---------- | ------------ | ------ | ---- | ------------------------------- |
| token      | 验证密钥     | string | 是   | 唯一验证凭证，泄露有风险        |
| wsAPIreqID | API请求ID    | int    | 否   | WS方式专用请求ID                |
| api        | API名        | string | 是   | 框架 或 httpApi 提供的可调用API |
| robot_wxid | 机器人微信ID | string | 否   |                                 |
| @param1    | 参数         | @      | 是   |                                 |
| @param2    | 参数         | @      | 是   |                                 |
| .......    | ......       | ...... |      |                                 |

#### GET 例子

> 值请使用URL编码处理

```
http://192.168.0.167:233?token=7k7k&api=SendTextMsg&robot_wxid=wxid_eu05e13ld28822&to_wxid=wxid_l5ltfi4zxv9a22&msg=%E4%BD%A0%E5%A5%BD
```

#### POST 例子

> 若无法确保不会出现影响JSON解析的值，请使用USC2编码处理

```
POST http://192.168.0.167:8863 HTTP/1.1`
`User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36`
`{`
` "token": "7k7k",`
` "api": "SendTextMsg",`
` "robot_wxid": "wxid_eu05e13ld28822",`
` "to_wxid": "wxid_l5ltfi4zxv9a22",`
` "msg": "\u4f60\u597d"`
`}
```

#### WebSocket 例子

> 若无法确保不会出现影响JSON解析的值，请使用USC2编码处理

```json
{
"token": "7k7k",
"api": "SendTextMsg",
"robot_wxid": "wxid_eu05e13ld28822",
 "to_wxid": "wxid_l5ltfi4zxv9a22",
"msg": "\u4f60\u597d",
"wsAPIreqID": 786
}
```

> wsAPIreqID使用介绍：
> 由于 WebSocket 并非应答式协议
> 您需要自建缓存池等方式 通过wsAPIreqID标识取回API返回的数据
> 在API请求json中 添加wsAPIreqID标识，如 "wsAPIreqID": 786
> 在返回数据json中 会返回相同的wsAPIreqID标识，如 "wsAPIreqID": 786
> wsAPIreqID标识 生成规律没有限制 可随意发挥
> 没有返回值的API 可以忽略不写

------

#### API调用后返回json，固定参数如下

**Code**：httpApi 插件返回 API接口调用状态码

> 与API功能成功无关
> 如：SendTextMsg（发消息）调用成功 但 未发出，亦返回0

**Result**：httpApi 插件返回 API接口调用状态说明
**wsAPIreqID**：httpApi 插件返回 API调用 WS请求标识，仅WS方式调用时存在
**根据API返回类型返回以下值（VLW 框架返回数据）：**

| 返回值类型 | JSON路径名 | 类型描述               |
| ---------- | ---------- | ---------------------- |
| **null**   |            | 没有返回值             |
| **str**    | ReturnStr  | 文本型内容（USC2编码） |
| **bool**   | ReturnBool | 逻辑值返回             |
| **int**    | ReturnInt  | 整数型内容             |
| **json**   | ReturnJson | JSON内容               |

```json
// [null] HTTP(GET/POST)示例
{
    "Code": 0, // 状态 0成功 -1未在白名单 -2token有误 -3api有误 -4参数有误 -97其他错误 -98调用方式有误 -99数据解析失败 -100未知错误 200——299具体含义请参考调用API的注释
    "Result": "OK"  // 状态说明，如失败的原因
}
// [null] WebSocket示例
{
    "Code": 0, // 状态 0成功 -1未在白名单 -2token有误 -3api有误 -4参数有误 -97其他错误 -98调用方式有误 -99数据解析失败 -100未知错误 -200 至 -299具体含义请参考调用API的注释
    "Result": "OK",  // 状态说明，如失败的原因
  "wsAPIreqID": 786  // API请求ID
}
// [str] HTTP(GET/POST)示例
{
    "Code": 0, // 状态 0成功 -1未在白名单 -2token有误 -3api有误 -4参数有误 -97其他错误 -98调用方式有误 -99数据解析失败 -100未知错误 200——299具体含义请参考调用API的注释
    "Result": "OK",  // 状态说明，如失败的原因
    "ReturnStr": "\u4f60\u597d"
}
// [str] WebSocket示例
{
    "Code": 0, // 状态 0成功 -1未在白名单 -2token有误 -3api有误 -4参数有误 -97其他错误 -98调用方式有误 -99数据解析失败 -100未知错误 200——299具体含义请参考调用API的注释
    "Result": "OK",  // 状态说明，如失败的原因
    "ReturnStr": "\u4f60\u597d",
  "wsAPIreqID": 786  // API请求ID
}
// [bool] HTTP(GET/POST)示例
{
    "Code": 0, // 状态 0成功 -1未在白名单 -2token有误 -3api有误 -4参数有误 -97其他错误 -98调用方式有误 -99数据解析失败 -100未知错误 200——299具体含义请参考调用API的注释
    "Result": "OK",  // 状态说明，如失败的原因
    "ReturnBool": true
}
// [bool] WebSocket示例
{
    "Code": 0, // 状态 0成功 -1未在白名单 -2token有误 -3api有误 -4参数有误 -97其他错误 -98调用方式有误 -99数据解析失败 -100未知错误 200——299具体含义请参考调用API的注释
    "Result": "OK",  // 状态说明，如失败的原因
    "ReturnBool": true,
  "wsAPIreqID": 786  // API请求ID
}
// [int] HTTP(GET/POST)示例
{
    "Code": 0, // 状态 0成功 -1未在白名单 -2token有误 -3api有误 -4参数有误 -97其他错误 -98调用方式有误 -99数据解析失败 -100未知错误 200——299具体含义请参考调用API的注释
    "Result": "OK",  // 状态说明，如失败的原因
    "ReturnInt": 123
}
// [int] WebSocket示例
{
    "Code": 0, // 状态 0成功 -1未在白名单 -2token有误 -3api有误 -4参数有误 -97其他错误 -98调用方式有误 -99数据解析失败 -100未知错误 200——299具体含义请参考调用API的注释
    "Result": "OK",  // 状态说明，如失败的原因
    "ReturnInt": 123,
  "wsAPIreqID": 786  // API请求ID
}
// [json] HTTP(GET/POST)示例
{
    "Code": 0, // 状态 0成功 -1未在白名单 -2token有误 -3api有误 -4参数有误 -97其他错误 -98调用方式有误 -99数据解析失败 -100未知错误 200——299具体含义请参考调用API的注释
    "Result": "OK",  // 状态说明，如失败的原因
    "ReturnJson": {"o":true}
}
// [json] WebSocket示例
{
    "Code": 0, // 状态 0成功 -1未在白名单 -2token有误 -3api有误 -4参数有误 -97其他错误 -98调用方式有误 -99数据解析失败 -100未知错误 200——299具体含义请参考调用API的注释
    "Result": "OK",  // 状态说明，如失败的原因
    "ReturnJson": {"o":true},
  "wsAPIreqID": 786  // API请求ID
}
```

# 消息回调

### 1.WebSocket 方式

> #### 此方式在开启状态下，推送优先级高于 POST 方式

#### 地址格式：`http://IP 或 域名:端口号/`

> 端口可以随意设置，但需要自行开放安全组

例子：
本地：`ws://127.0.0.1:8863/`
内网：`ws://192.168.0.167:8863/`
外网：`ws://xpw.yuque.com:8863/`

> 若外网调用，请确保您的网络拥有公网IP，否则无法使用
> 服务器：进入供应商控制台查看，找不到或有问题请联系您的供应商客服
> 本 地：需联系您当地网络供应商开通公网IP
> 提示：只能提示这么多，此类问题恕不解答

#### 返回内容需为 JSON 格式

> 由于 WebSocket 并非应答式协议
> 消息推送JSON内 wsMCBreqID 是此条消息的唯一标识
> 请使用wsMCBreqID标识 请尽快返回，否则消息将一直卡住。

```json
{
  "wsMCBreqID": 786,  // WS 消息推送标识
  "Code": -1  // 消息处理方式：-1中断推送 0忽略此消息 1拦截此消息
}
```

------

### 2.HTTP → POST 方式

#### 设置的地址为外网时，请确保您网络拥有公网IP，否则无法使用

> 服务器：进入供应商控制台查看，找不到或有问题请联系您的供应商客服
> 本 地：需联系您当地网络供应商开通公网IP
> 提示：只能提示这么多，此类问题恕不解答

------

#### 采用post方法访问你设置的地址，地址可以设置多个，消息会逐一推送直到返回值为 -1 或 JON解析失败中断推送

> 地址填写以 http:// 开头

------

#### 返回内容需为 JSON 格式

> 消息推送后请尽快返回，否则消息将一直卡住。
> 为防止解析错误，JSON 中不应出现其他字段，请严格遵守示例

```json
{
  "Code": "-1"  // 消息处理方式：-1中断推送 0忽略此消息 1拦截此消息
}
```

------

### 推送内容为 JSON 格式

- 推送内容 与 VLW官方 易语言模板 保持一致，请参考
- 根据插件采用 SDK 版本不同，推送内容也不同
- 值为USC2编码

#### Login（新的账号登录成功/下线）

```json
{
    "sdkVer": 6,  // SDK版本号
    "Event": "Login", // 事件（易语言模板的子程序名）
    "content": {
        "type": 1,  // 0 登录成功 / 1 即将离线
        "Wxid": "",  // 登录/离线 的Wxid
          "robot_type": 0  // 来源微信类型 0 正常微信 / 1 企业微信
    }  // 内容（易语言模板的参数名）
}
{
    "sdkVer": 6,  // SDK版本号
    "Event": "Login", // 事件（易语言模板的子程序名）
    "content": {
        "type": 1,  // 0 登录成功 / 1 即将离线
        "Wxid": "",  // 登录/离线 的Wxid
          "robot_type": 0  // 来源微信类型 0 正常微信 / 1 企业微信
    },  // 内容（易语言模板的参数名）
    "wsMCBreqID": 786  // WS 消息推送标识
}
```

#### EventGroupChat（群消息事件）

```json
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventGroupChat", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "type": 1,  // 1/文本消息 3/图片消息 34/语音消息  42/名片消息  43/视频 47/动态表情 48/地理位置  49/分享链接  2001/红包  2002/小程序  2003/群邀请 更多请参考常量表
        "from_group": "",  // 来源群号
        "from_group_name": "",  // 来源群名称
        "from_wxid": "",  // 具体发消息的群成员id
        "from_name": "",  // 具体发消息的群成员昵称
        "msg": "",  // 消息内容
        "msg_source": {
            "atuserlist": [{
                "wxid": "wxid_6ungmd6wtdh521",
                "nickname": "@测试",
                "position_from": 1,
                "position_to": 7
            }]
        },  // 附带JSON属性（群消息有艾特人员时，返回被艾特信息）
        "clientid": 0,  // 企业微信可用
          "robot_type": 0,  // 来源微信类型 0 正常微信 / 1 企业微信
        "msg_id": 0  // 消息ID
    }  // 内容（易语言模板的参数名）
}
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventGroupChat", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "type": 1,  // 1/文本消息 3/图片消息 34/语音消息  42/名片消息  43/视频 47/动态表情 48/地理位置  49/分享链接  2001/红包  2002/小程序  2003/群邀请 更多请参考常量表
        "from_group": "",  // 来源群号
        "from_group_name": "",  // 来源群名称
        "from_wxid": "",  // 具体发消息的群成员id
        "from_name": "",  // 具体发消息的群成员昵称
        "msg": "",  // 消息内容
        "msg_source": {
            "atuserlist": [{
                "wxid": "wxid_6ungmd6wtdh521",
                "nickname": "@测试",
                "position_from": 1,
                "position_to": 7
            }]
        },  // 附带JSON属性（群消息有艾特人员时，返回被艾特信息）
        "clientid": 0,  // 企业微信可用
          "robot_type": 0,  // 来源微信类型 0 正常微信 / 1 企业微信
        "msg_id": 0  // 消息ID
    },  // 内容（易语言模板的参数名）
    "wsMCBreqID": 786  // WS 消息推送标识
}
```

#### EventPrivateChat（私聊消息事件）

```json
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventPrivateChat", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "type": 1,  // 1/文本消息 3/图片消息 34/语音消息  42/名片消息  43/视频 47/动态表情 48/地理位置  49/分享链接  2001/红包  2002/小程序  2003/群邀请  更多请参考常量表
        "from_wxid": "",  // 来源用户ID
        "from_name": "",  // 来源用户昵称
        "msg": "",  // 消息内容
        "clientid": 0,  // 企业微信可用
          "robot_type": 0,  // 来源微信类型 0 正常微信 / 1 企业微信
        "msg_id": 0  // 消息ID
    }  // 内容（易语言模板的参数名）
}
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventPrivateChat", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "type": 1,  // 1/文本消息 3/图片消息 34/语音消息  42/名片消息  43/视频 47/动态表情 48/地理位置  49/分享链接  2001/红包  2002/小程序  2003/群邀请  更多请参考常量表
        "from_wxid": "",  // 来源用户ID
        "from_name": "",  // 来源用户昵称
        "msg": "",  // 消息内容
        "clientid": 0,  // 企业微信可用
          "robot_type": 0,  // 来源微信类型 0 正常微信 / 1 企业微信
        "msg_id": 0  // 消息ID
    },  // 内容（易语言模板的参数名）
    "wsMCBreqID": 786  // WS 消息推送标识
}
```

#### EventDeviceCallback（设备回调事件）

```json
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventDeviceCallback", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "type": 1,  // 消息类型
        "msg": "",  // 消息内容
        "to_wxid": "",  // 接收用户ID
        "to_name": "",  // 接收用户昵称
        "clientid": 0,  // 企业微信可用
          "robot_type": 0,  // 来源微信类型 0 正常微信 / 1 企业微信
                "msg_id": 0  // 消息ID
    }  // 内容（易语言模板的参数名）
}
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventDeviceCallback", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "type": 1,  // 消息类型
        "msg": "",  // 消息内容
        "to_wxid": "",  // 接收用户ID
        "to_name": "",  // 接收用户昵称
        "clientid": 0,  // 企业微信可用
          "robot_type": 0,  // 来源微信类型 0 正常微信 / 1 企业微信
          "msg_id": 0  // 消息ID
    },  // 内容（易语言模板的参数名）
    "wsMCBreqID": 786  // WS 消息推送标识
}
```

#### EventFrieneVerify（好友请求事件）

```json
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventFrieneVerify", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "type": 1,  // 添加方式 请参考常量表
        "from_wxid": "",  // 请求者wxid
        "from_name": "",  // 请求者昵称
        "v1": "",
        "v2": "",
        "json_msg": {
            "to_wxid": "wxid_eu05e13ld28822",
            "to_name": "譬如朝露",
            "msgid": 1111250493,
            "from_wxid": "wxid_6ungmd6wtdh521",
            "from_nickname": "??[奸笑]??",
            "v1": "xxxxx",
            "v2": "xxxxx",
            "sex": 1,
            "from_content": "我是??[奸笑]??",
            "headimgurl": "http://wx.qlogo.cn/xxxxx",
            "type": 3
        },  // 友验证信息JSON(群内添加时，包含群id) (名片推荐添加时，包含推荐人id及昵称) (微信号、手机号搜索等添加时,具体JSON结构请查看日志）
          "robot_type": 0  // 来源微信类型 0 正常微信 / 1 企业微信
    }  // 内容（易语言模板的参数名）
}
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventFrieneVerify", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "type": 1,  // 添加方式 请参考常量表
        "from_wxid": "",  // 请求者wxid
        "from_name": "",  // 请求者昵称
        "v1": "",
        "v2": "",
        "json_msg": {
            "to_wxid": "wxid_eu05e13ld28822",
            "to_name": "譬如朝露",
            "msgid": 1111250493,
            "from_wxid": "wxid_6ungmd6wtdh521",
            "from_nickname": "??[奸笑]??",
            "v1": "xxxxx",
            "v2": "xxxxx",
            "sex": 1,
            "from_content": "我是??[奸笑]??",
            "headimgurl": "http://wx.qlogo.cn/xxxxx",
            "type": 3
        },  // 友验证信息JSON(群内添加时，包含群id) (名片推荐添加时，包含推荐人id及昵称) (微信号、手机号搜索等添加时,具体JSON结构请查看日志）
          "robot_type": 0  // 来源微信类型 0 正常微信 / 1 企业微信
    },  // 内容（易语言模板的参数名）
    "wsMCBreqID": 786  // WS 消息推送标识
}
```

#### EventGroupNameChange（群名称变动事件）

```json
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventGroupNameChange", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "from_group": "",  // 群号
        "new_name": "",  // 新群名
        "old_name": "",  // 旧群名
        "from_name": "",  // 操作者昵称
        "clientid": 0,  // 企业微信可用
          "robot_type": 0  // 来源微信类型 0 正常微信 / 1 企业微信
    }  // 内容（易语言模板的参数名）
}
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventGroupNameChange", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "from_group": "",  // 群号
        "new_name": "",  // 新群名
        "old_name": "",  // 旧群名
        "from_name": "",  // 操作者昵称
        "clientid": 0,  // 企业微信可用
          "robot_type": 0  // 来源微信类型 0 正常微信 / 1 企业微信
    },  // 内容（易语言模板的参数名）
    "wsMCBreqID": 786  // WS 消息推送标识
}
```

#### EventGroupMemberAdd（群成员增加事件 PS: 新人进群）

```json
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventGroupMemberAdd", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "from_group": "",  // 群号
        "from_group_name": "",  // 群名
        "guest": [{
            "wxid": "wxid_e6shncy2hlzm32",
            "username": "测试"
        }],  // 新人
        "inviter": {
            "wxid": "wxid_6ungmd6wtdh521",
            "username": "??[奸笑]??"
        },  // 邀请者
        "clientid": 0,  // 企业微信可用
          "robot_type": 0  // 来源微信类型 0 正常微信 / 1 企业微信
    }  // 内容（易语言模板的参数名）
}
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventGroupMemberAdd", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "from_group": "",  // 群号
        "from_group_name": "",  // 群名
        "guest": [{
            "wxid": "wxid_e6shncy2hlzm32",
            "username": "测试"
        }],  // 新人
        "inviter": {
            "wxid": "wxid_6ungmd6wtdh521",
            "username": "??[奸笑]??"
        },  // 邀请者
        "clientid": 0,  // 企业微信可用
          "robot_type": 0  // 来源微信类型 0 正常微信 / 1 企业微信
    },  // 内容（易语言模板的参数名）
    "wsMCBreqID": 786  // WS 消息推送标识
}
```

#### EventGroupMemberDecrease（群成员减少事件 PS: 群成员退出）

```json
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventGroupMemberDecrease", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "from_group": "",  // 群号
        "from_group_name": "",  // 群名
        "to_wxid": "",  // 被操作者wxid
        "to_name": "",  // 被操作者昵称
        "time": "",  // 操作时间
        "clientid": 0,  // 企业微信可用
          "robot_type": 0  // 来源微信类型 0 正常微信 / 1 企业微信
    }  // 内容（易语言模板的参数名）
}
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventGroupMemberDecrease", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "from_group": "",  // 群号
        "from_group_name": "",  // 群名
        "to_wxid": "",  // 被操作者wxid
        "to_name": "",  // 被操作者昵称
        "time": "",  // 操作时间
        "clientid": 0,  // 企业微信可用
          "robot_type": 0  // 来源微信类型 0 正常微信 / 1 企业微信
    },  // 内容（易语言模板的参数名）
    "wsMCBreqID": 786  // WS 消息推送标识
}
```

#### EventInvitedInGroup（被邀请入群事件 PS: 企业微信不传递此事件）

```json
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventInvitedInGroup", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "from_wxid": "",  // 邀请机器人入群的id
        "msg":  {
            "inviter_wxid": "wxid_e6shncy2hlzm32",
            "inviter_nickname": "测试",
            "group_headimgurl": "https://u.weixin.qq.com/xxxxx",
            "group_name": "测试群",
            "invite_url": "https://support.weixin.qq.com/xxxxx"
        },  // 内容
          "robot_type": 0  // 来源微信类型 0 正常微信 / 1 企业微信  后面可能会传递，预留参数
    }  // 内容（易语言模板的参数名）
}
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventInvitedInGroup", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "from_wxid": "",  // 邀请机器人入群的id
        "msg":  {
            "inviter_wxid": "wxid_e6shncy2hlzm32",
            "inviter_nickname": "测试",
            "group_headimgurl": "https://u.weixin.qq.com/xxxxx",
            "group_name": "测试群",
            "invite_url": "https://support.weixin.qq.com/xxxxx"
        },  // 内容
          "robot_type": 0  // 来源微信类型 0 正常微信 / 1 企业微信  后面可能会传递，预留参数
    },  // 内容（易语言模板的参数名）
    "wsMCBreqID": 786  // WS 消息推送标识
}
```

#### EventQRcodePayment（面对面收款事件）

```json
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventQRcodePayment", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "to_wxid": "",  // 收款者wxid
        "payer_wxid": "",  // 付款者wxid
        "payer_nickname": "",  // 付款者昵称
        "payer_pay_id": "",
        "received_money_index": "",
        "money": "",  // 金额
        "total_money": "",
        "remark": "",  // 备注
        "scene_desc": "",
        "scene": "",  // -1 扫码后退出 / 1 已扫码，未付款 / 2 付款完成 / 3 付款完成后的日志 / 4 付款完成后的日志（商家版），这里重点说明一下，如要做收款播报，只需要判断等于2或者等3的时候就可以了
        "time": ""
    }  // 内容（易语言模板的参数名）
}
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventQRcodePayment", // 事件（易语言模板的子程序名）
    "content": {
        "robot_wxid": "",  // 机器人账号id
        "to_wxid": "",  // 收款者wxid
        "payer_wxid": "",  // 付款者wxid
        "payer_nickname": "",  // 付款者昵称
        "payer_pay_id": "",
        "received_money_index": "",
        "money": "",  // 金额
        "total_money": "",
        "remark": "",  // 备注
        "scene_desc": "",
        "scene": "",  // -1 扫码后退出 / 1 已扫码，未付款 / 2 付款完成 / 3 付款完成后的日志 / 4 付款完成后的日志（商家版），这里重点说明一下，如要做收款播报，只需要判断等于2或者等3的时候就可以了
        "time": ""
    },  // 内容（易语言模板的参数名）
    "wsMCBreqID": 786  // WS 消息推送标识
}
```

#### EventDownloadFile（文件下载结束事件）

```json
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventDownloadFile", // httpApi定义事件
    "content": {
        "downloadIde": 1, // 下载标识
        "url": "", // 文件下载直链
        "savePath": "", // 文件完整路径
        "fileName": "", // 文件名
        "fileSize": 15247252, // 文件总大小 单位 B
        "downloadSize": 15247252, // 已下载大小 单位 B
        "schedule": 1, // 下载进度 0.00 - 1.00
        "speed": 0, // 平均速度 KB/S 实时速度 在下载时有值 下载完成时归零
        "downloadType": 3, // 下载状态 0 未开始 1 开始下载 2 下载中 3 结束下载 -1 下载错误
        "api": "", // 调用API
        "robot_wxid": "", // 机器人ID
        "to_wxid": "" // 对方的ID（好友/群ID/公众号ID）
    }  // 内容（易语言模板的参数名）
}
{
    "sdkVer": 6,  // SDK版本号
    "Event": "EventDownloadFile", // httpApi定义事件
    "content": {
        "downloadIde": 1, // 下载标识
        "url": "", // 文件下载直链
        "savePath": "", // 文件完整路径
        "fileName": "", // 文件名
        "fileSize": 15247252, // 文件总大小 单位 B
        "downloadSize": 15247252, // 已下载大小 单位 B
        "schedule": 1, // 下载进度 0.00 - 1.00
        "speed": 0, // 平均速度 KB/S 实时速度 在下载时有值 下载完成时归零
        "downloadType": 3, // 下载状态 0 未开始 1 开始下载 2 下载中 3 结束下载 -1 下载错误
        "api": "", // 调用API
        "robot_wxid": "", // 机器人ID
        "to_wxid": "" // 对方的ID（好友/群ID/公众号ID）
    },  // 内容（易语言模板的参数名）
    "wsMCBreqID": 786  // WS 消息推送标识
}
```

#### EventGroupEstablish（创建新的群聊事件，企业微信不传递此事件）

```json
{
  "sdkVer": 6,  // SDK版本号
  "Event": "EventGroupEstablish", // 事件（易语言模板的子程序名）
  "content": {
    "robot_wxid": "",  // 机器人账号id
    "from_group": "",  // 来源群号
    "robot_type": 0  // 来源微信类型 0 正常微信 / 1 企业微信
  }  // 内容（易语言模板的参数名）
}
{
  "sdkVer": 6,  // SDK版本号
  "Event": "EventGroupEstablish", // 事件（易语言模板的子程序名）
  "content": {
    "robot_wxid": "",  // 机器人账号id
    "from_group": "",  // 来源群号
    "robot_type": 0  // 来源微信类型 0 正常微信 / 1 企业微信
  },  // 内容（易语言模板的参数名）
    "wsMCBreqID": 786  // WS 消息推送标识
}
```

# 常量表

### 添加类型

- **微xin号搜索 3**

- **QQ号搜索 12**

- **群聊 14**

- **手机通讯录 15**

- **名片分享 17**

- **附近人 18**

- **摇一摇 29**

- **扫一扫 30**

  ### 消息类型

- **文本 1**

- **图片消息 3**

- **语音消息 34**

- **好友验证 37**

- **名片推荐 42**

- **视频消息 43**

- **动态表情 47**

- **位置消息 48**

- **分享链接 49**

- **转账消息 2000**

- **红包消息 2001**

- **小程序 2002**

- **群邀请 2003**

- **接收文件 2004**

- **撤回消息 2005**

- **系统消息 10000**

- **服务通知 10002**

# 常用端口号

### 请避开常用端口号，否则很可能导致无法使用。

#### 端口号标识了一个主机上进行通信的不同的应用程序。

**1.HTTP协议代理服务器常用端口号：80/8080/3128/8081/9098**
**2.SOCKS代理协议服务器常用端口号：1080**
**3.FTP（文件传输）协议代理服务器常用端口号：21**
**4.Telnet（远程登录）协议代理服务器常用端口号：23**
**HTTP服务器，默认端口号为80/tcp（木马Executor开放此端口）**
**HTTPS（securely transferring web pages）服务器，默认端口号为443/tcp 443/udp**
**Telnet（不安全的文本传送），默认端口号为23/tcp（木马Tiny Telnet Server所开放的端口）**
**FTP，默认的端口号为21/tcp（木马Doly Trojan、Fore、Invisible FTP、WebEx、WinCrash和Blade Runner所开放的端口）**
**TFTP（Trivial File Transfer Protocol），默认端口号为69/udp**
**SSH（安全登录）、SCP（文件传输）、端口号重定向，默认的端口号为22/tcp**
**SMTP Simple Mail Transfer Protocol（E-mail），默认端口号为25/tcp（木马Antigen、Email Password Sender、Haebu Coceda、Shtrilitz Stealth、WinPC、WinSpy都开放这个端口）**
**POP3 Post Office Protocol（E-mail），默认端口号为110/tcp**
**Webshpere应用程序，默认端口号为9080**
**webshpere管理工具，默认端口号9090**
**JBOSS，默认端口号为8080**
**TOMCAT，默认端口号为8080**
**WIN2003远程登录，默认端口号为3389**
**Symantec AV/Filter for MSE,默认端口号为 8081**
**Oracle 数据库，默认的端口号为1521**
**ORACLE EMCTL，默认的端口号为1158**
**Oracle XDB（XML 数据库），默认的端口号为8080**
**Oracle XDB FTP服务，默认的端口号为2100**
**MS SQL\*SERVER数据库server，默认的端口号为1433/tcp 1433/udp**
**MS SQL\*SERVER数据库monitor，默认的端口号为1434/tcp 1434/udp**



# API.....

