// 判断服务器上面有没有 upload 目录。如果没有就创建这个目录，如果有的话不做操作
const fs = require("fs");

function isExis(path) {
	fs.stat(path, (err, data) => {
		if (err) {
			// 执行创建目录
			mkdir(path);
			console.log(path + "创建目录成功");
			return;
		}
		if (data.isDirectory()) {
			console.log(path + "目录存在");
		} else {
			/*
fs.unlink()方法用于从文件系统中删除文件或符号链接。
此函数不适用于目录，因此建议使用fs.rmdir()删除目录。
(文件跟文件名不能相同)
        */
			fs.unlink(path, (err) => {
				if (!err) {
					mkdir(path);
				}
			});
		}
	});
}
function mkdir(dir) {
	fs.mkdir(dir, (err) => {
		if (err) {
			console.log(err);
			return;
		}
	});
}
module.exports = isExis;
