const fs = require("fs");
const path = require("path");

const readDir = (url) => {
	var temPath = [];
	const dirInfo = fs.readdirSync(url);

	dirInfo.map((item) => {
		const location = path.join(url, item);
		const info = fs.statSync(location);

		if (info.isDirectory()) {
			readDir(location);
		} else {
			temPath.push(path.basename(location));
		}
	});
	return temPath;
};

module.exports = readDir;
