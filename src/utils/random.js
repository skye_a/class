function getRangeCode(len=6) {
	var  orgStr='ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
    // abcdefghijklmnopqrstuvwxyz
	let returnStr="";
	for (var i = 0; i < len; i++) {
		returnStr+=orgStr.charAt(Math.floor((Math.random()*orgStr.length)));
	}
	return returnStr;
}
module.exports=getRangeCode