const db = require("../utils/mysql.js");
const ff = require("../utils/forEachFile");
const v = require("../utils/verify");

// 展示当前课程下的所有以及上传的文件
exports.queryAllFile = (req, res) => {
     // 验证参数
    if(v(req,["work_id"],res)!=0){
        return;
    }
	if (!req.user.teacher) {
		return res.sd("学生不要违规操作");
	}
	const work_id = req.query.work_id;
	console.log(`查询作业[${work_id}]下的所有上传的文件`);

	const sql = `select courseName,work_name from course,work,work_course where course.id=work_course.course_id and work_course.work_id=work.id and work_id=?`;

	db.query(sql, [work_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		// 名被占用
		if (results.length > 0) {
			var names = [];
			names.push(
				...ff(
					"./stuWork/" +
						results[0].courseName +
						"/" +
						results[0].work_name
				)
			);
			res.sd(
				{ message: "查询成功", count: names.length, results: names },
				0
			);
		}
	});
};

// 展示老师上传的文件
exports.lqueryAllFile = (req, res) => {
     // 验证参数
    if(v(req,["work_id"],res)!=0){
        return;
    }
	const work_id = req.query.work_id;
	console.log(`查询[${work_id}]下的所有老师上传的文件`);

	const sql = `select courseName,work_name from course,work,work_course where course.id=work_course.course_id and work_course.work_id=work.id and work_id=?`;

	db.query(sql, [work_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		// 名被占用
		if (results.length > 0) {
			var names = [];
			names.push(
				...ff(
					"./stuWork/" +
						results[0].courseName +
						"/" +
						results[0].work_name +
						"/target/"
				)
			);
			res.sd(
				{ message: "查询成功", count: names.length, results: names },
				0
			);
		}
	});
};
