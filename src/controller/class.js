const fs = require("fs");
const db = require("../utils/mysql.js");
const rando = require("../utils/random.js");
const v = require("../utils/verify");
const v2 = require("../utils/verify2");

// 创建课程
exports.createCourse = (req, res) => {
	if (!req.user.teacher) {
		return res.sd("学生不要违规操作");
	}
	// 验证参数
	if (v2(req, ["courseName"], res) != 0) {
		return;
	}

	const courseName = req.body.courseName;

	console.log(`创建课程[${courseName}]`);

	const sql = `select * from course where courseName=?`;

	db.query(sql, [courseName], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		const isExit = require("../utils/isExistPath");
		var sdir = "./stuWork/" + courseName;
		// 首先判断此次课程的文件夹是否创建
		isExit(sdir);

		// 名被占用
		if (results.length > 0) {
			return res.sd("课程已经存在,不用重复创建");
		}

		const sql = `insert into course (teacher, invite,courseName) values(?,?,?)`;
		const invite = rando();
		// 注意：req 对象上的 user 属性，是 Token 解析成功，express-jwt 中间件帮我们挂载上去的
		db.query(sql, [req.user.id, invite, courseName], (err, results) => {
			// 1. 执行 SQL 语句失败
			if (err) return res.sd(err);

			// 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
			if (results.affectedRows !== 1) {
				return res.sd("创建失败, 联系后端, 错误日志:\n", results);
			}
			res.sd({ message: "创建成功", invite, courseName }, 0);
		});
	});
};

// 根据当前老师查找课程
exports.lqueryCourse = (req, res) => {
	if (!req.user.teacher) {
		return res.sd("学生不要违规操作");
	}
	console.log(`查找课程`);

	const sql = `select *,(select count(*) from stu_course where stu_course.course_id=course.id) as stuCount,(select count(*) from work_course where work_course.course_id=course.id) as workCount from course where teacher=?`;

	db.query(sql, [req.user.id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};

// 创建作业(弃用)
exports.createWork = (req, res) => {
	if (!req.user.teacher) {
		return res.sd("学生不要违规操作");
	}
	const isExit = require("../utils/isExistPath");

	const workName = req.body.workName;
	const course_id = req.body.course_id;
	const deadline = req.body.deadline;
	if (!req.body.workName || !req.body.course_id || !req.body.deadline) {
		return res.sd("参数验证失败! 请检查workName course_id deadline");
	}

	// 首先获得课程名
	var sql1 = "select courseName from course where id =?";
	var courseName = "";
	db.query(sql1, [course_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		// 名被占用
		if (results.length > 0) {
			courseName = results[0].courseName;
		}

		var sdir = "./stuWork/" + courseName + "/" + workName;
		// 首先判断此次作业的文件夹是否创建
		isExit(sdir);
		var sdir = "./stuWork/" + courseName + "/" + workName + "/target";
		isExit(sdir);
		console.log(`创建[${course_id}]作业[${workName}]`);

		const sql = `select * from work where work_name=?`;

		db.query(sql, [workName], function (err, results) {
			// 执行 SQL 语句失败
			if (err) {
				return res.sd(err.message);
			}
			// 名被占用
			if (results.length > 0) {
				return res.sd(`[${workName}]作业已经存在,不用重复创建`);
			}

			// 作业插入数据库
			const sql = `insert into work (work_name, createTime, deadline) values (?,now(),?)`;
			db.query(sql, [workName, deadline], (err, results) => {
				// 1. 执行 SQL 语句失败
				if (err) return res.sd(err);
				// 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
				if (results.affectedRows !== 1) {
					return res.sd("创建失败, 联系后端, 错误日志:\n", results);
				}

				// 插入关系
				const sql3 =
					"insert into work_course(work_id, course_id) value ((select id from work where work_name=?),?)";
				db.query(sql3, [workName, course_id], (err, results) => {
					// 1. 执行 SQL 语句失败
					if (err) return res.sd(err);
					// 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
					if (results.affectedRows !== 1) {
						return res.sd(
							"创建失败, 联系后端, 错误日志:\n",
							results
						);
					}
					res.sd({ message: "创建成功" }, 0);
				});
			});
		});
	});
};

// ============================

// 根据当前学生查找课程
exports.queryCourse = (req, res) => {
	console.log(`查找当前学生的所有课程`);

	const sql = `select course.id 'course_id',course.invite,course.courseName,teacher.id 'teacher_id',teacher.name teacher_name from teacher,course,stu_course where course.id=stu_course.course_id and course.teacher=teacher.id and stu_id =?`;

	db.query(sql, [req.user.id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};
// # 查询该课程下所有作业并显示当前学生完成情况 并且显示作业过期情况
exports.queryWorkStu = (req, res) => {
	// 验证参数
	if (v(req, ["course_id"], res) != 0) {
		return;
	}
	const course_id = req.query.course_id;
	console.log(`查找课程[${course_id}]的所有作业,和自己的完成情况`);

	const sql = `select work.id work_id,work.work_name,work.createTime,work.deadline,if(? in (select work_stu.stu_id),1,0) as done,if(deadline>now(),1,0) as valid from work left join work_stu on work.id = work_stu.work_id where work.id in (select work_course.work_id from work_course where course_id=?) ;`;

	db.query(sql, [req.user.id, course_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			if (err.message.match("ER_BAD_FIELD_ERROR").length != 0) {
				return res.sd("查询失败!");
			}
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};
// 学生加入课堂
exports.intoCourse = (req, res) => {
	// 验证参数
	if (v2(req, ["invite"], res) != 0) {
		return;
	}
	const invite = req.body.invite;
	console.log(`学生加入课堂${invite}`);

	const sql = `select * from stu_course where course_id in (select id from course where invite=? ) and stu_id=?`;

	db.query(sql, [invite, req.user.id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		// 名被占用
		if (results.length > 0) {
			return res.sd("已经加入了,不用重复加入");
		}

		const sql = `insert into stu_course (course_id,stu_id) values ((select id from course where invite = ?),?);`;

		db.query(sql, [invite, req.user.id], function (err, results) {
			// 执行 SQL 语句失败
			if (err) {
				return err.name == "Error"
					? res.sd("课程邀请码错误!")
					: res.sd(`错误日志:${err}`);
			}
			// 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
			if (results.affectedRows !== 1) {
				return res.sd("加入失败, 联系后端, 错误日志:\n", results);
			}
			res.sd({ message: "加入课程成功" }, 0);
		});
	});
};

// 学生提交作业
exports.submit = (req, res) => {
	// 验证参数
	if (v2(req, ["work_id"], res) != 0) {
		return;
	}
	const work_id = req.body.work_id;

	console.log(`学生提交作业${work_id}`);
	const sql = `select * from work_stu where work_id=? and stu_id=?`;

	db.query(sql, [work_id, req.user.id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}

		if (results.length > 0) {
			return res.sd("已经提交了,请等待老师查阅");
		}
		const sql = `insert into work_stu (work_id, stu_id) value(?,?);`;

		db.query(sql, [work_id, req.user.id], function (err, results) {
			// 执行 SQL 语句失败
			if (err) {
				return res.sd(err.message);
			}
			// 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
			if (results.affectedRows !== 1) {
				return res.sd("提交失败, 联系后端, 错误日志:\n", results);
			}
			res.sd({ message: "提交成功" }, 0);
		});
	});
};

// 查询自己该课程的得分情况
exports.queryScore = (req, res) => {
	// 验证参数
	if (v(req, ["course_id"], res) != 0) {
		return;
	}
	const course_id = req.query.course_id;

	console.log(`学生查询成绩${course_id}`);
	const sql = `select user.username,user.name,user.class, course.courseName, sign,(select count(*) from sign where course_id=?)as Allsign, work,(select count(*) from work,work_course where work.id=work_course.work_id and work_course.course_id=?)as AllWork, score,(select ((select course.sign_sc from course where id=?)*(select count(*) from sign where course_id=?)+(select course.work_sc from course where id=?)*(select count(*) from work,work_course where work.id=work_course.work_id and work_course.course_id=?)*2) )as fullMark,(select 100*score/fullMark)as relative from score,course,user where score.stu_id=user.id and course.id=score.course_id and stu_id=? and course_id=?`;

	db.query(
		sql,
		[
			course_id,
			course_id,
			course_id,
			course_id,
			course_id,
			course_id,
			req.user.id,
			course_id,
		],
		function (err, results) {
			// 执行 SQL 语句失败
			if (err) {
				return res.sd(err.message);
			}

			res.sd({ message: results }, 0);
		}
	);
};

// =============================

// 查找指定课程下的所有学生
exports.queryStu = (req, res) => {
	// 验证参数
	if (v(req, ["course_id"], res) != 0) {
		return;
	}
	const course_id = req.query.course_id;
	console.log(`查找学习课程[${course_id}]的所有学生`);

	const sql = `select user.id,name,username,class from user,stu_course,course where stu_id=user.id and course_id=course.id and course.id=?`;

	db.query(sql, [course_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			if (err.message.match("ER_BAD_FIELD_ERROR").length != 0) {
				return res.sd("查询失败!");
			}
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};

// 查询该课程下的所有作业[课程id]
exports.queryWork = (req, res) => {
	// 验证参数
	if (v(req, ["course_id"], res) != 0) {
		return;
	}
	const course_id = req.query.course_id;
	console.log(`查找课程[${course_id}]的所有作业`);

	const sql = `select work.id,work.work_name,work.createTime,work.deadline from work_course,course,work where work.id=work_course.work_id and course.id=work_course.course_id and course.id=? order by work.id desc`;

	db.query(sql, [course_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			if (err.message.match("ER_BAD_FIELD_ERROR").length != 0) {
				return res.sd("查询失败!");
			}
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};

// # 查询该课程下所有作业,以及提交的人数和课程下的总人数
exports.lqueryWorkStu = (req, res) => {
	// 验证参数
	if (v(req, ["course_id"], res) != 0) {
		return;
	}
	const course_id = req.query.course_id;
	console.log(
		`查找课程[${course_id}]的所有作业, 以及已经提交的数量和总学生数量`
	);

	const sql = `select work.id,work.work_name,work.createTime,work.deadline,(select count(*) from work_stu where work_id=work.id) as workCount,(select count(*) from stu_course where stu_course.course_id =?) as stuCount from work where work.id in (select work_course.work_id from work_course where course_id=?)`;

	db.query(sql, [course_id, course_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			if (err.message.match("ER_BAD_FIELD_ERROR").length != 0) {
				return res.sd("查询失败!");
			}
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};

// 查询该作业学生的完成情况
exports.queryWorkDone = (req, res) => {
	// 验证参数
	if (v(req, ["work_id"], res) != 0) {
		return;
	}
	const work_id = req.query.work_id;
	console.log(`查找学生对作业[${work_id}]的完成情况`);

	const sql = `select user.id,user.username,user.name,user.class,if(id in (select stu_id from work_stu where work_id=?),1,0)as done from stu_course,user where stu_course.stu_id=user.id and course_id = (select work_course.course_id from work_course,work where work.id=work_course.work_id and work_id=?)`;

	db.query(sql, [work_id, work_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			if (err.message.match("ER_BAD_FIELD_ERROR").length != 0) {
				return res.sd("查询失败!");
			}
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};

//  查询完成了该作业的学生 [作业id]
exports.queryStuWorkDone = (req, res) => {
	// 验证参数
	if (v(req, ["work_id"], res) != 0) {
		return;
	}
	const work_id = req.query.work_id;
	console.log(`查找完成作业[${work_id}]的学生`);

	const sql = `select user.id,user.name,user.username,user.class from user,work,work_stu where user.id=work_stu.stu_id and work.id=work_stu.work_id and work.id=?`;

	db.query(sql, [work_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			if (err.message.match("ER_BAD_FIELD_ERROR").length != 0) {
				return res.sd("查询失败!");
			}
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};

//  查询未完成该作业的学生 [作业id]
exports.queryStuWorkNone = (req, res) => {
	// 验证参数
	if (v(req, ["work_id"], res) != 0) {
		return;
	}
	const work_id = req.query.work_id;
	console.log(`查找未完成作业[${work_id}]的学生`);

	const sql = `select user.id,user.name,user.username,user.class from stu_course,user where stu_course.stu_id=user.id and  course_id in (select course_id from work_course where work_id=?) and id not in (select stu_id from work_stu where work_id=?)`;

	db.query(sql, [work_id, work_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};

// 老师批改此次作业
exports.correct = (req, res) => {

	// 验证参数
	if (v(req, ["work_id","docName","good"], res) != 0) {
		return;
	}
	const work_id = req.query.work_id;
	const docName = req.query.docName;
	const good = req.query.good;

	console.log(`批改[${docName}]作业`);
	// 获取文件名中的学号
	var re = /(?<=).*?(?=-)/;
	var username = docName.match(re)[0];
	var stu_id = "";
	console.log("#", username);

	// 首先获取学生id

	const sql = `select id from user where username=?`;
	// 注意：req 对象上的 user 属性，是 Token 解析成功，express-jwt 中间件帮我们挂载上去的
	db.query(sql, [username], (err, results) => {
		// 1. 执行 SQL 语句失败
		if (err) return res.sd(err);
		try {
			stu_id = results[0].id;
		} catch {
			return res.sd("批改失败! 联系后端 失败日志:" + results);
		}

		if (good == "true") {
			// 作业合格 成绩表中加分
			const sql = `update score set score=score+5 where stu_id=? and course_id=(select course_id from work_course where work_id=?)`;
			// 注意：req 对象上的 user 属性，是 Token 解析成功，express-jwt 中间件帮我们挂载上去的
			db.query(sql, [stu_id, work_id], (err, results) => {
				// 1. 执行 SQL 语句失败
				if (err) return res.sd(err);

				// 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
				if (results.affectedRows !== 1) {
					return res.sd(
						"批改失败,意外错误, 联系后端, 错误日志:\n" + results
					);
				}
				res.sd({ message: "合格作业批改成功" }, 0);
			});
		} else {
			//先删除学生上传的文件
			// 先查询课程名称
			const sql = `select courseName,(select work_name from work where id=?) as workName from course where id=(select work_course.course_id from work_course where work_id=?)`;
			// 注意：req 对象上的 user 属性，是 Token 解析成功，express-jwt 中间件帮我们挂载上去的
			db.query(sql, [work_id, work_id], (err, results) => {
				// 1. 执行 SQL 语句失败
				if (err) return res.sd(err);

				var courseName = results[0].courseName;
				var workName = results[0].workName;

				fs.unlink(
					"./stuWork/" + courseName + "/" + workName + "/" + docName,
					function (err) {
						if (err) {
							console.log(err);
							res.sd("删除文件时发生错误! 文件不存在,或者[docName]错误!");
                            return;
						}
						console.log("删除不合格作业文件成功");
					}
				);

				// 删除学生提交记录
				const sql = `delete from work_stu where work_id=? and stu_id=?`;
				// 注意：req 对象上的 user 属性，是 Token 解析成功，express-jwt 中间件帮我们挂载上去的
				db.query(sql, [work_id, stu_id], (err, results) => {
					// 1. 执行 SQL 语句失败
					if (err) return res.sd(err);

					// 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
					if (results.affectedRows !== 1) {
						return res.sd(
							"批改失败,意外错误, 联系后端, 错误日志:\n",
							results
						);
					}
					res.sd({ message: "不合格作业批改成功" }, 0);
				});
			});
		}
	});
};

//  查询该课程下所有学生的详细成绩
exports.queryAllScore = (req, res) => {
	// 验证参数
	if (v(req, ["course_id"], res) != 0) {
		return;
	}
	const course_id = req.query.course_id;
	console.log(`查找所有[${course_id}]课程的学生的详细成绩`);

	const sql = `select user.username,user.name,user.class, course.courseName, sign,(select count(*) from sign where course_id=?)as Allsign, work,(select count(*) from work,work_course where work.id=work_course.work_id and work_course.course_id=?)as AllWork, score,(select ((select course.sign_sc from course where id=?)*(select count(*) from sign where course_id=?)+(select course.work_sc from course where id=?)*(select count(*) from work,work_course where work.id=work_course.work_id and work_course.course_id=?)*2) )as fullMark,(select 100*score/fullMark)as relative from score,course,user where score.stu_id=user.id and course.id=score.course_id and stu_id in (select stu_id from stu_course where stu_course.course_id=?) and course_id=?`;

	db.query(sql, [course_id,course_id,course_id,course_id,course_id,course_id,course_id,course_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};