// 使用 exports 对象，分别向外共享如下 路由处理函数 ：
/**
 * 在这里定义和用户相关的路由处理函数，供 /router/user.js 模块进行调用
 */
// 引入数据库实例
const db = require("../utils/mysql.js");

// 用这个包来生成 Token 字符串
const jwt = require("jsonwebtoken");

// 引入密码加密模块
const bcrypt = require("bcryptjs");
const { use } = require("../../routes/upload.js");

// 注册用户的处理函数(学生)
exports.regUser = (req, res) => {
	// 接收表单数据
	const userinfo = req.body;
	if (
		!userinfo.username ||
		!userinfo.password ||
		!userinfo.name ||
		!userinfo.class
	) {
		return res.sd("注册失败! 检查参数是否为空!");
	}
	console.log(`学生注册:${userinfo.username}`);
	if (userinfo == undefined) {
		return res.send("信息不合法");
	}

	// 判断数据是否合法
	// 首先判断数据库中是否已经有这个用户
	const sql = `select * from user where username=?`;

	db.query(sql, [userinfo.username], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		// 用户名被占用
		if (results.length > 0) {
			return res.sd("你已经注册,请前往登陆");
		}
		// TODO: 用户名可用，

		// 注册成功  往数据库里面存数据

		// 加密密码
		// 对用户的密码,进行 bcrype 加密，返回值是加密之后的密码字符串,(密码明文, 加密盐长度)
		userinfo.password = bcrypt.hashSync(userinfo.password, 10);

		const sql = "insert into user set ?";
		db.query(
			sql,
			{
				username: userinfo.username,
				password: userinfo.password,
				name: userinfo.name,
				class: userinfo.class,
			},
			function (err, results) {
				if (err) return res.send({ status: 1, message: err.message });
				// SQL 语句执行成功，但影响行数不为 1
				if (results.affectedRows !== 1) {
					return res.sd("注册用户失败，未知错误,联系后端人员");
				}
				// 注册成功
				res.sd("注册成功", 0);
			}
		);
	});
};

// 登录的处理函数(学生)
exports.login = (req, res) => {
	const userinfo = req.body;
	if (!userinfo.password || !userinfo.username) {
		return res.sd("登陆失败! 检查参数是否为空!");
	}
	const sql = `select * from user where username=?`;

	// 查询数据库中的信息
	db.query(sql, userinfo.username, function (err, results) {
		// 执行 SQL 语句失败
		if (err) return res.sd(err);
		// 执行 SQL 语句成功，但是查询到数据条数不等于 1
		if (results.length !== 1) return res.sd("登录失败！");
		// TODO：判断用户输入的登录密码是否和数据库中的密码一致
		// 拿着用户输入的密码,和数据库中存储的密码进行对比
		const compareResult = bcrypt.compareSync(
			userinfo.password,
			results[0].password
		);

		// 如果对比的结果等于 false, 则证明用户输入的密码错误
		if (!compareResult) {
			return res.sd("登录失败！密码错误");
		} else {
			// 通过 ES6 的高级语法，快速剔除 密码 和 头像 的值：
			// 剔除完毕之后，user 中只保留了用户的 id, username, nickname, email 这四个属性的值
			const user = { ...results[0], password: "", teacher: false };

			const config = require("../utils/jwt.js");

			// 生成 Token 字符串
			const tokenStr = jwt.sign(user, config.jwtSecretKey, {
				expiresIn: "7d", // token 有效期为 10 个小时
			});
			console.log(`学生登陆成功:${userinfo.username}`);

			res.send({
				status: 0,
				message: "登录成功！",
				// 为了方便客户端使用 Token，在服务器端直接拼接上 Bearer 的前缀
				authorization: "Bearer " + tokenStr,
			});
		}
	});
};

// 注册用户的处理函数(老师)
exports.lregUser = (req, res) => {
	// 接收表单数据
	const userinfo = req.body;
	if (!userinfo.username || !userinfo.password || !userinfo.name) {
		return res.sd("注册失败! 检查参数是否为空!");
	}
	console.log(`老师注册:${userinfo.username}`);
	if (userinfo == undefined) {
		return res.send("信息不合法");
	}
	// 判断数据是否合法
	// 首先判断数据库中是否已经有这个用户

	const sql = `select * from teacher where username=?`;

	db.query(sql, [userinfo.username], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		// 用户名被占用
		if (results.length > 0) {
			return res.sd("你已经注册,请前往登陆");
		}
		// TODO: 用户名可用，

		// 注册成功  往数据库里面存数据

		// 加密密码
		// 对用户的密码,进行 bcrype 加密，返回值是加密之后的密码字符串,(密码明文, 加密盐长度)
		userinfo.password = bcrypt.hashSync(userinfo.password, 10);

		const sql = "insert into teacher set ?";
		db.query(
			sql,
			{
				username: userinfo.username,
				password: userinfo.password,
				name: userinfo.name,
			},
			function (err, results) {
				if (err) return res.send({ status: 1, message: err.message });
				// SQL 语句执行成功，但影响行数不为 1
				if (results.affectedRows !== 1) {
					return res.sd("注册用户失败，未知错误,联系后端人员");
				}
				// 注册成功
				res.sd("注册成功", 0);
			}
		);
	});
};

// 登录的处理函数(老师)
exports.llogin = (req, res) => {
	const userinfo = req.body;
	if (!userinfo.password || !userinfo.username) {
		return res.sd("登陆失败! 检查参数是否为空!");
	}
	const sql = `select * from teacher where username=?`;

	// 查询数据库中的信息
	db.query(sql, userinfo.username, function (err, results) {
		// 执行 SQL 语句失败
		if (err) return res.sd(err);
		// 执行 SQL 语句成功，但是查询到数据条数不等于 1
		if (results.length !== 1) return res.sd("登录失败！");
		// TODO：判断用户输入的登录密码是否和数据库中的密码一致
		// 拿着用户输入的密码,和数据库中存储的密码进行对比
		const compareResult = bcrypt.compareSync(
			userinfo.password,
			results[0].password
		);

		// 如果对比的结果等于 false, 则证明用户输入的密码错误
		if (!compareResult) {
			return res.sd("登录失败！密码错误");
		} else {
			// 通过 ES6 的高级语法，快速剔除 密码 和 头像 的值：
			// 剔除完毕之后，user 中只保留了用户的 id, username, nickname, email 这四个属性的值
			const user = { ...results[0], password: "", teacher: true };

			const config = require("../utils/jwt.js");

			// 生成 Token 字符串
			const tokenStr = jwt.sign(user, config.jwtSecretKey, {
				expiresIn: "7d", // token 有效期为 7d
			});
			console.log(`老师登陆成功:${userinfo.username}`);

			res.send({
				status: 0,
				message: "登录成功！",
				// 为了方便客户端使用 Token，在服务器端直接拼接上 Bearer 的前缀
				authorization: "Bearer " + tokenStr,
			});
		}
	});
};
