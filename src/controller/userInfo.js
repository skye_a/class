// 获取用户基本信息的处理函数

// 导入数据库操作模块
const db = require("../utils/mysql.js");

// 获取用户信息[学生]
exports.getUserInfo = (req, res) => {
	const sql = `select id, name, username, class from user where id=?`;
	// 注意：req 对象上的 user 属性，是 Token 解析成功，express-jwt 中间件帮我们挂载上去的
	db.query(sql, req.user.id, (err, results) => {
        console.log(`查询[${req.user.id}]学生信息`);
		// 1. 执行 SQL 语句失败
		if (err) return res.sd(err);

		// 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
		if (results.length !== 1) return res.sd("获取用户信息失败！");

		// 3. 将用户信息响应给客户端
		res.send({
			status: 0,
			message: "获取学生基本信息成功！",
			data: results[0],
		});
	});
};

// 获取用户信息[老师]
exports.getlUserInfo = (req, res) => {
	const sql = `select id, name, username from teacher where id=?`;
	// 注意：req 对象上的 user 属性，是 Token 解析成功，express-jwt 中间件帮我们挂载上去的
	db.query(sql, req.user.id, (err, results) => {
        console.log(`查询[${req.user.id}]老师信息`);
		// 1. 执行 SQL 语句失败
		if (err) return res.sd(err);

		// 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
		if (results.length !== 1) return res.sd("获取用户信息失败！");

		// 3. 将用户信息响应给客户端
		res.send({
			status: 0,
			message: "获取老师基本信息成功！",
			data: results[0],
		});
	});
};