const db = require("../utils/mysql.js");
const v = require("../utils/verify");
const v2 = require("../utils/verify2");


// 老师发起签到
exports.createSign = (req, res) => {
	// 验证参数
	if (v2(req, ["course_id", "deadline"], res) != 0) {
		return;
	}
	const deadline = req.body.deadline;
	const course_id = req.body.course_id;
	console.log(`在[${course_id}]课,创建签到,截止到[${deadline}]`);

	const sql = `insert into sign (course_id, createTime,deadline) value(?,now(),?)`;
	// 注意：req 对象上的 user 属性，是 Token 解析成功，express-jwt 中间件帮我们挂载上去的
	db.query(sql, [course_id, deadline], (err, results) => {
		// 1. 执行 SQL 语句失败
		if (err) return res.sd(err);

		// 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
		if (results.affectedRows !== 1) {
			return res.sd("创建失败, 联系后端, 错误日志:\n", results);
		}
		res.sd({ message: "创建成功" }, 0);
	});
};

// 查询当前课程所有未过期的签到
exports.querySign = (req, res) => {
	// 验证参数
	if (v(req, ["course_id"], res) != 0) {
		return;
	}
	const course_id = req.query.course_id;
	console.log(`查找${course_id}课程下的所有有效签到`);

	const sql = `select id, createTime, deadline from sign where course_id=? and deadline>now();`;

	db.query(sql, [course_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};

// 查询当前作业的所有签到信息
exports.queryAllSign = (req, res) => {
	// 验证参数
	if (v(req, ["course_id"], res) != 0) {
		return;
	}
	const course_id = req.query.course_id;
	console.log(`查找${course_id}课程下的所有签到`);

	const sql = `select id, createTime, deadline,if(deadline>now(),1,0) as valed from sign where course_id=?`;

	db.query(sql, [course_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};

// 查询当前课的签到情况
exports.queryAllSignState = (req, res) => {
	// 验证参数
	if (v(req, ["course_id", "sign_id"], res) != 0) {
		return;
	}
	const course_id = req.query.course_id;
	const sign_id = req.query.sign_id;
	console.log(`查找${course_id}课程下的签到情况`);

	const sql = `select id,name,username,class,if(id in (select sign_stu.stu_id from sign_stu where sign_stu.sign_id=?),1,0) as done from user,stu_course where user.id=stu_course.stu_id and stu_course.course_id=?`;

	db.query(sql, [sign_id, course_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};

// 查询已经签到的学生
exports.querySignDone = (req, res) => {
	// 验证参数
	if (v(req, ["sign_id"], res) != 0) {
		return;
	}
	const sign_id = req.query.sign_id;
	console.log(`查找[${sign_id}] 签到了的学生`);

	const sql = `select id, name, username, class from user where id in (select stu_id from sign_stu where sign_id=?)`;

	db.query(sql, [sign_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};

// 查询未签到的学生
exports.querySignNone = (req, res) => {
	// 验证参数
	if (v(req, ["sign_id"], res) != 0) {
		return;
	}
	const sign_id = req.query.sign_id;
	console.log(`查找[${sign_id}] 未签到的学生`);

	const sql = `select user.id,user.name,user.username,user.class from user where class in (select course_id from work_course where work_id in(select course_id from sign where id=?)) and id != (select stu_id from sign_stu where sign_id=?)`;

	db.query(sql, [sign_id, sign_id], function (err, results) {
		// 执行 SQL 语句失败
		if (err) {
			return res.sd(err.message);
		}
		res.sd({ message: "查询成功", count: results.length, results }, 0);
	});
};


// ===============================

// 学生进行签到
exports.toSign = (req, res) => {
	// 验证参数
	if (v2(req, ["sign_id"], res) != 0) {
		return;
	}
	if (req.user.teacher) {
		return res.sd("老师不必签到");
	}
	const sign_id = req.body.sign_id;
	console.log(`学生进行签到[${sign_id}]`);

	const sql = `insert into sign_stu(stu_id, sign_id) value (?,?);`;
	// 注意：req 对象上的 user 属性，是 Token 解析成功，express-jwt 中间件帮我们挂载上去的
	db.query(sql, [req.user.id, sign_id], (err, results) => {
		// 1. 执行 SQL 语句失败
		if (err) return res.sd(err);

		// 2. 执行 SQL 语句成功，但是查询到的数据条数不等于 1
		if (results.affectedRows !== 1) {
			return res.sd("创建失败, 联系后端, 错误日志:\n", results);
		}
		res.sd({ message: "签到成功" }, 0);
	});
};
